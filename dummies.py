import logging
import os
import sys
from datetime import datetime

import tkinter as tk

from main import Repetition, User, UserId, get_user_dir, Filenames, Exercise, WordId, ExerciseId, \
    VocabularyEntryRepetitionConfig, write_repetitions_results, RepetitionsResultsType, write_exercises, ExercisesType, \
    write_repetitions_config, RepetitionsConfigType, VocabularyType, RepetitionErrorType, load_exercises, \
    ExercisesRepetitionConfigType, ExerciseRepetitionConfig, RevisionStatus, write_exercises_repetition_config

dummy_user = User(uid=UserId(0), name='Dummy')


def create_dummy_repetition_results():
    repetition_results = RepetitionsResultsType({
        WordId(1): [
            Repetition(date=datetime(2020, 9, 1, 2, 15), n_attempts=1, was_last_attempt_successful=False),
            Repetition(date=datetime(2020, 9, 2, 3, 16), n_attempts=1, was_last_attempt_successful=True),
            Repetition(date=datetime(2020, 9, 3, 3, 16), n_attempts=2, was_last_attempt_successful=True,
                       error_types=RepetitionErrorType.ACCENT),
            Repetition(date=datetime(2020, 9, 4, 3, 16), n_attempts=1, was_last_attempt_successful=True),
            Repetition(date=datetime(2020, 9, 5, 3, 16), n_attempts=1, was_last_attempt_successful=True),
        ],
        WordId(2): [
            Repetition(date=datetime(2020, 10, 20, 10, 59), n_attempts=5, was_last_attempt_successful=True),
            Repetition(date=datetime(2020, 10, 21), n_attempts=9, was_last_attempt_successful=True),
        ],
    })

    filepath = os.path.join(get_user_dir(dummy_user), Filenames.repetitions_filename)
    write_repetitions_results(filepath=filepath, repetition_results=repetition_results)
    # with open(file=os.path.join(get_user_dir(dummy_user), Filenames.repetitions_filename), mode='wb') as file:
    #     pickle.dump(obj=repetition_results, file=file)


def create_dummy_exercises():
    exercises = ExercisesType({
        ExerciseId(1): Exercise(eid=ExerciseId(1), name='Ex1', words={WordId(1), WordId(2)}),
        ExerciseId(2): Exercise(eid=ExerciseId(2), name='Ex2', words={WordId(3), WordId(4)}),
    })

    write_exercises(exercises=exercises, user=dummy_user)

    ex_conf: ExercisesRepetitionConfigType = ExercisesRepetitionConfigType({
        eid: ExerciseRepetitionConfig(eid=eid, revision_status=RevisionStatus.REVISE) for eid in exercises
    })

    write_exercises_repetition_config(exercises_repetition_config=ex_conf,
                                      user=dummy_user)


def create_dummy_repetitions_config():
    repetitions_config = RepetitionsConfigType({
        WordId(1): VocabularyEntryRepetitionConfig(wid=WordId(1), is_revised=True),
        WordId(2): VocabularyEntryRepetitionConfig(wid=WordId(2), is_revised=True),
        WordId(3): VocabularyEntryRepetitionConfig(wid=WordId(3), is_revised=False),
        # WordId(23): VocabularyEntryRepetitionConfig(wid=WordId(23), is_revised=True),
    })

    write_repetitions_config(repetitions_config=repetitions_config,
                             filepath=os.path.join(get_user_dir(dummy_user), Filenames.repetitions_config_filename))


# def set_whole_vocabulary_to_be_revised(repetitions_config: RepetitionsConfigType, vocabulary: VocabularyType):
#     for wid in vocabulary:
#         if wid not in repetitions_config:
#             repetitions_config[wid] = VocabularyEntryRepetitionConfig(wid=wid, is_revised=True)


class DummyApp(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.logger = self.set_logger()
        self.mode = 'DEBUG'

    @classmethod
    def set_logger(cls):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s')  # %(name)s
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        return logger


if __name__ == '__main__':
    if not os.path.isdir(get_user_dir(dummy_user)):
        os.makedirs(get_user_dir(dummy_user))

    # create_dummy_repetition_results()
    # create_dummy_exercises()
    # ex = load_exercises(dummy_user)
    create_dummy_repetitions_config()

    pass

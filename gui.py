# NOTE: https://stackoverflow.com/a/17960528/492221
import tkinter as tk

rows = 4
columns = 2
width = 50


class MainWindow(tk.Toplevel):
    def __init__(self, root, **kwargs):
        super().__init__(root, **kwargs)

        frame = self.frame = tk.Frame(self)
        self.frame.grid(row=1, columnspan=2, padx=2, pady=2, sticky=tk.N + tk.E + tk.S + tk.W)

        self.text_area = tk.Canvas(self.frame, background="black", width=400, height=500,
                                   scrollregion=(0, 0, 1200, 800))
        self.hscroll = tk.Scrollbar(self.frame, orient=tk.HORIZONTAL, command=self.text_area.xview)
        self.vscroll = tk.Scrollbar(self.frame, orient=tk.VERTICAL, command=self.text_area.yview)
        self.text_area['xscrollcommand'] = self.hscroll.set
        self.text_area['yscrollcommand'] = self.vscroll.set

        self.text_area.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
        self.hscroll.grid(row=1, column=0, sticky=tk.E + tk.W)
        self.vscroll.grid(row=0, column=1, sticky=tk.N + tk.S)

        self._widgets = []

        for row in range(rows):
            current_row = []
            for column in range(columns):
                label = tk.Label(self.text_area, text="",
                                 borderwidth=0, width=width)
                label.grid(row=row, column=column, sticky="nsew", padx=1, pady=1)
                current_row.append(label)
            self._widgets.append(current_row)


root = tk.Tk()
root.withdraw()

top = MainWindow(root, bg='red')

root.mainloop()

# window = MainWindow()

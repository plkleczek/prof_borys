import collections
import configparser
import dataclasses
import logging
import os
import tkinter as tk
import copy
from abc import ABC, abstractmethod
from logging import Logger
from tkinter import ttk
from typing import Dict, NamedTuple, Callable, List, Set, Optional, Collection, Any, Tuple

from tkintertable import TableCanvas, TableModel, Filtering
from tkinter import *

from dummies import DummyApp
from main import VocabularyType, read_vocabulary, VocabularyConfig, ExercisesType, ExerciseId, Exercise, \
    VocabularyEntry, Difficulty, WordId, load_exercises, User, write_exercises, get_lowest_id, RevisionStatus, \
    ExercisesRepetitionConfigType, ExerciseRepetitionConfig, load_exercises_repetition_config, \
    write_exercises_repetition_config, update_exercises_repetition_config

revision_state_combobox_labels = collections.OrderedDict({
    RevisionStatus.REVISE: 'Revise',
    RevisionStatus.SKIP: 'Skip',
    RevisionStatus.USE_PARENT_SETTINGS: 'Use parent settings',
})


class ExerciseTreeFrame(ttk.Frame):
    def __init__(self, master,
                 exercises: ExercisesType,
                 exercises_repetition_config: ExercisesRepetitionConfigType,
                 item_clicked_hook: Callable,
                 **kwargs):
        super().__init__(master, **kwargs)

        self.exercises = exercises
        self.exercises_repetition_config = exercises_repetition_config
        self.item_clicked_hook = item_clicked_hook

        self.selected_exercise_iid: Optional[str] = None

        self.tree = ttk.Treeview(self)
        self.tree.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.tree['columns'] = ("id", "n_words", "revision_status")
        self.eid_col_inx = self.tree['columns'].index("id")

        self.tree.column("#0", width=270, minwidth=100)
        self.tree.column("id", width=40, minwidth=40, stretch=tk.NO)
        self.tree.column("n_words", width=40, minwidth=40, stretch=tk.NO)
        self.tree.column("revision_status", width=100, minwidth=100, stretch=tk.NO)

        self.tree.heading("#0", text="Name", anchor=tk.W)
        self.tree.heading("id", text="ID", anchor=tk.W)
        self.tree.heading("n_words", text="n", anchor=tk.W)
        self.tree.heading("revision_status", text="RevStatus", anchor=tk.W)

        self.build_exercise_tree()

        self.open_children()

        ybar = tk.Scrollbar(self, orient=tk.VERTICAL,
                            command=self.tree.yview)
        ybar.pack(side=tk.RIGHT, fill=tk.Y)
        self.tree.configure(yscrollcommand=ybar.set)
        # FIXME: Make the scrollbar visible.

        self.tree.bind('<Button-1>', self.__on_treeview_item_clicked)

    def __on_treeview_item_clicked(self, event):
        item_iid = self.tree.identify('item', event.x, event.y)
        self.selected_exercise_iid = item_iid
        if not item_iid and self.tree.get_children():
            self.selected_exercise_iid = self.tree.get_children()[-1]
            self.tree.focus(self.selected_exercise_iid)
            self.tree.selection_set(self.selected_exercise_iid)

        self.item_clicked_hook(event)

    def build_exercise_tree(self):
        def get_exercises_sorted_alphabetically(exercises: Collection[Exercise]) -> List[Exercise]:
            return list(sorted(exercises, key=lambda item: item.name))

        # Remove all tree's content.
        self.tree.delete(*self.tree.get_children())

        parents = {eid: set() for eid in self.exercises}
        for eid, ex in self.exercises.items():
            for sub_ex_id in ex.subexercises:
                parents[sub_ex_id].add(eid)

        root_exercises = [ex for ex in self.exercises.values() if not parents[ex.eid]]

        iids: Set[str] = set()

        class InsertionEntry(NamedTuple):
            this_exercise: Exercise
            parent_iid: str

        # visited: Dict[ExerciseId] = {}
        queue: List[InsertionEntry] = [
            InsertionEntry(this_exercise=ex, parent_iid='') for ex in
            get_exercises_sorted_alphabetically(root_exercises)
        ]
        while queue:
            exercise, parent_iid = queue.pop(0)
            this_iid = f'{parent_iid}_{exercise.eid}'
            iids.add(this_iid)
            queue.extend([
                InsertionEntry(this_exercise=ex, parent_iid=this_iid)
                for ex in self.exercises.values() if ex.eid in exercise.subexercises
            ])

            status = self.exercises_repetition_config[exercise.eid].revision_status
            self.tree.insert(parent=parent_iid, index='end', iid=this_iid,
                             text=exercise.name,
                             values=(exercise.eid, len(exercise.words), revision_state_combobox_labels[status]))

        # Check if the entry still exists ('cause it could disappear due to changes in the hierarchy).
        if self.selected_exercise_iid in iids:
            self.tree.focus(self.selected_exercise_iid)
            self.tree.selection_set(self.selected_exercise_iid)
        else:
            self.selected_exercise_iid = None

        self.open_children(parent_iid='')

    def open_children(self, parent_iid=''):
        """
        Recursively expand all tree nodes.
        """
        self.tree.item(parent_iid, open=True)  # open parent
        for child in self.tree.get_children(parent_iid):
            self.open_children(child)  # recursively open children

    @classmethod
    def eid_from_iid(cls, iid: str) -> ExerciseId:
        return ExerciseId(int(iid.split('_')[-1]))

    @property
    def selected_eid(self) -> Optional[ExerciseId]:
        return self.selected_exercise.eid if self.selected_exercise is not None else None

    @property
    def selected_exercise(self) -> Optional[Exercise]:
        """
        The initial state of the given exercise.
        """

        return self.exercises[
            self.eid_from_iid(self.selected_exercise_iid)] if self.selected_exercise_iid else None


class ExerciseConfigWindow:
    """Basic test frame for the table"""

    config_path = os.path.join('data', 'config.ini')

    def __init__(self, tk_controller: tk.Tk, logger: Logger, user: User, app, main_frame):
        # TODO: Use events (i.e., observer pattern?)
        self.app = app
        self.main_frame = main_frame

        # A mapping of Listbox entries to their respective WordId-s.
        self.exercise_words_binding: Dict[str, WordId] = {}

        config = configparser.ConfigParser()
        config.read(self.config_path)

        self.user = user
        self.exercises: ExercisesType = load_exercises(self.user)
        self.exercises_repetition_config: ExercisesRepetitionConfigType = load_exercises_repetition_config(self.user)
        update_exercises_repetition_config(exercises_repetition_config=self.exercises_repetition_config,
                                           exercises=self.exercises,
                                           user=self.user)

        tree_frame_width = 400
        padding = 10

        exercise_frame_width = 500 + padding
        main_frame_width = tree_frame_width + exercise_frame_width
        frame_height = 500

        self.window = tk.Toplevel(tk_controller)
        self.window.title('Exercises configuration')
        self.window.minsize(width=main_frame_width, height=frame_height)
        self.window.maxsize(width=main_frame_width, height=frame_height)

        screen_width = tk_controller.winfo_screenwidth()
        screen_height = tk_controller.winfo_screenheight()
        self.window.geometry(f'{main_frame_width}x{frame_height}+{screen_width // 4}+{screen_height // 4}')

        self.window.protocol("WM_DELETE_WINDOW", self.__on_closing)

        # tree_frame = ttk.Frame(self.window, height=500, width=tree_frame_width)
        self.tree_frame = ExerciseTreeFrame(
            self.window,
            exercises=self.exercises,
            exercises_repetition_config=self.exercises_repetition_config,
            item_clicked_hook=self.__on_treeview_item_clicked,
            height=500, width=tree_frame_width,
        )
        # words_frame.pack(fill=BOTH, expand=True)
        # words_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.tree_frame.grid(column=0, row=0, sticky='nesw')

        # == < Exercise summary > ==

        exercise_frame = ttk.Frame(self.window, height=500, width=exercise_frame_width)
        exercise_frame.grid(column=1, row=0, sticky='nesw', padx=padding)

        exercise_summary_frame = ttk.LabelFrame(exercise_frame, text='Summary', height=200, width=exercise_frame_width)
        exercise_summary_frame.grid(column=0, row=0, sticky='nesw', padx=padding, pady=padding)

        exercise_name_label = ttk.Label(exercise_summary_frame, text='Name:')
        exercise_name_label.grid(column=0, row=0, sticky='es', pady=padding)
        self.exercise_name_text_label = ttk.Label(exercise_summary_frame, text='(-)')
        self.exercise_name_text_label.grid(column=1, row=0, sticky='ws', pady=padding)

        exercise_revision_status_label = ttk.Label(exercise_summary_frame, text='Revision status:')
        exercise_revision_status_label.grid(column=0, row=1, sticky='s', pady=padding)
        self.exercise_revision_status_combobox = ttk.Combobox(
            exercise_summary_frame,
            state="readonly",
            values=list(revision_state_combobox_labels.values())
        )
        self.exercise_revision_status_combobox.bind("<<ComboboxSelected>>",
                                                    self.__on_exercise_revision_status_combobox_selected)
        self.exercise_revision_status_combobox.grid(column=1, row=1, sticky='s', pady=padding)

        # ====

        self.edit_hierarchy_button = ttk.Button(exercise_frame, text="Edit hierarchy",
                                                state=tk.DISABLED)
        self.edit_hierarchy_button.grid(column=0, row=1, sticky='nw', padx=padding)

        def edit_hierarchy_button_click(event):
            hierarchy_win = ExerciseHierarchyEditor(tk_controller=self.app,
                                                    logger=self.app.logger,
                                                    user=user,
                                                    exercises=self.exercises,
                                                    edited_exercise=self.selected_exercise,
                                                    update_parent_hook=self.update_content
                                                    )

        self.edit_hierarchy_button.bind('<Button-1>', edit_hierarchy_button_click)

        # == </ Exercise summary > ==

        self.tree_frame.tree.bind('<Button-1>', self.__on_treeview_item_clicked)

        # ====

        self.window.grid_columnconfigure(0, minsize=tree_frame_width, weight=1)
        self.window.grid_columnconfigure(1, minsize=exercise_frame_width, weight=0)
        self.window.grid_rowconfigure(0, minsize=400, weight=1)

        # ====

        # Set the focus on dialog window (needed on Windows).
        self.window.focus_set()
        # Make sure events only go to our dialog.
        # self.window.grab_set()
        # Make sure dialog stays on top of its parent window (if needed).
        self.window.transient(tk_controller)

    def update_content(self):
        self.tree_frame.build_exercise_tree()

        self.edit_hierarchy_button.configure(
            state=tk.NORMAL if self.selected_exercise is not None else tk.DISABLED)

        # FIXME: Set the previously selected element.

    @property
    def selected_exercise(self) -> Optional[Exercise]:
        """
        The initial state of the given exercise.
        """

        return self.exercises[
            ExerciseTreeFrame.eid_from_iid(self.selected_exercise_iid)] if self.selected_exercise_iid else None

    def __on_treeview_item_clicked(self, event):
        item_iid = self.tree_frame.tree.identify('item', event.x, event.y)
        self.selected_exercise_iid = item_iid
        if not item_iid and self.tree_frame.tree.get_children():
            self.selected_exercise_iid = self.tree_frame.tree.get_children()[-1]
            self.tree_frame.tree.focus(self.selected_exercise_iid)
            self.tree_frame.tree.selection_set(self.selected_exercise_iid)

        self.exercise_name_text_label['text'] = self.selected_exercise.name if self.selected_exercise else '(-)'
        status = self.exercises_repetition_config[
            self.selected_exercise.eid].revision_status if self.selected_exercise else \
            list(revision_state_combobox_labels.keys())[0]
        self.exercise_revision_status_combobox.current(
            list(revision_state_combobox_labels.keys()).index(status))
        self.exercise_revision_status_combobox['state'] = 'readonly' if self.selected_exercise else 'disabled'

        self.edit_hierarchy_button.configure(
            state=tk.NORMAL if self.selected_exercise is not None else tk.DISABLED)

    def __on_exercise_revision_status_combobox_selected(self, event):
        status = list(revision_state_combobox_labels.keys())[
            list(revision_state_combobox_labels.values()).index(self.exercise_revision_status_combobox.get())]
        self.exercises_repetition_config[self.selected_exercise.eid].revision_status = status

        write_exercises_repetition_config(exercises_repetition_config=self.exercises_repetition_config,
                                          user=self.user)
        self.tree_frame.build_exercise_tree()

    def __on_closing(self):
        self.app.load_user_data(self.user)
        self.main_frame.update_controls()
        self.window.destroy()


class AddRemoveListFrame(ttk.LabelFrame):
    BUTTON_FRAME_WIDTH = 60
    padding = 10
    SELECTED_EXERCISE_LIST_WIDTH = 300

    re_exercise_listbox_entry = re.compile(r'.*\(#(?P<eid>\d+)\)')

    def __init__(self, master, tk_controller,
                 exercises: ExercisesType,
                 exercise_tree_frame: ExerciseTreeFrame,
                 get_items_hook: Callable,
                 add_item_hook: Callable,
                 remove_item_hook: Callable,
                 **kwargs) -> None:
        """

        :param master:
        :param tk_controller:
        :param exercises:
        :param get_items_hook:
        :param add_item_hook:
        :param remove_item_hook:
        :param exercise_tree_frame:
        :param kwargs:
        """
        super().__init__(master, **kwargs)

        self.exercises = exercises
        self.exercise_tree_frame = exercise_tree_frame
        self.get_items_hook = get_items_hook
        self.add_item_hook = add_item_hook
        self.remove_item_hook = remove_item_hook

        self.selected_items: Set[ExerciseId] = set()
        # A mapping of Listbox entries to their respective "real" items.
        self.entries_to_items_binding: Dict[str, ExerciseId] = {}

        listbox_frame = ttk.Frame(self)
        listbox_frame.grid(column=1, row=0, sticky='nesw', padx=self.padding)

        self.listbox_values = tk.Variable()
        self.listbox = tk.Listbox(listbox_frame, height=5, width=20,
                                  listvariable=self.listbox_values,
                                  exportselection=False)
        self.listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # exercise_listbox.grid(row=0, column=0, columnspan=2, sticky='nesw')
        scrollbar = tk.Scrollbar(self.listbox)
        scrollbar.pack(side=tk.RIGHT, fill=tk.BOTH)

        # FIXME: Implement.
        # self.__update_exercises_list()

        self.listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.listbox.yview)
        self.listbox.pack(fill=tk.BOTH)

        def listbox_select(event):
            selection = event.widget.curselection()
            # FIXME: Implement.
            self.__listbox_select(selection)

        self.listbox.bind("<<ListboxSelect>>", listbox_select)

        self.listbox_selected_entry: Optional[str] = None

        button_frame = ttk.Frame(self, width=self.BUTTON_FRAME_WIDTH - self.padding)
        button_frame.controller = tk_controller
        button_frame.grid_propagate(False)
        # button_frame.pack(side=tk.LEFT)
        button_frame.grid(column=0, row=0, sticky='nesw', padx=self.padding)

        self.add_item_button = tk.Button(button_frame, text=">", state=tk.DISABLED)
        self.remove_item_button = tk.Button(button_frame, text="<", state=tk.DISABLED)

        self.add_item_button.grid(column=0, row=0)
        self.remove_item_button.grid(column=0, row=1, pady=self.padding)

        def add_item_button_click(event):
            self.__add_item_button_click()

        def remove_item_button_click(event):
            self.__remove_item_button_click()

        self.add_item_button.bind('<Button-1>', add_item_button_click)
        self.remove_item_button.bind('<Button-1>', remove_item_button_click)

        self.grid_propagate(False)

        # FIXME: Configure.
        self.grid_columnconfigure(0, minsize=self.BUTTON_FRAME_WIDTH, weight=0)
        self.grid_columnconfigure(1, minsize=self.SELECTED_EXERCISE_LIST_WIDTH, weight=1)
        self.grid_rowconfigure(0, minsize=200, weight=0)
        self.grid_rowconfigure(1, minsize=200, weight=1)

        self.__update_listbox()

    def __listbox_select(self, entry_ids: Tuple):
        selection: Tuple = self.listbox_values.get()
        if not selection:
            return

        self.listbox_selected_entry: str = selection[entry_ids[0]]
        eid: ExerciseId = self.__extract_exercise_id_from_listbox_entry(self.listbox_selected_entry)

        self.__update_listbox()

    def __extract_exercise_id_from_listbox_entry(self, entry: str) -> ExerciseId:
        m = self.re_exercise_listbox_entry.fullmatch(entry)
        if m:
            return ExerciseId(int(m.group('eid')))
        else:
            raise ValueError(f'Incorrect entry: {entry}')

    def update_buttons(self, main_selection: Any) -> None:
        """

        :param main_selection: The item selected on the main list.
        """
        # FIXME: Prevent cyclic dependencies etc.

        is_main_item_selected = main_selection is not None

        self.add_item_button.configure(
            state=tk.NORMAL if is_main_item_selected else tk.DISABLED)

        self.remove_item_button.configure(
            state=tk.NORMAL if self.listbox_selected_entry is not None else tk.DISABLED)

    def __add_item_button_click(self):
        if self.exercise_tree_frame.selected_exercise_iid is None:
            raise RuntimeError("No exercise selected.")

        self.selected_items.add(self.exercise_tree_frame.selected_eid)
        self.add_item_hook()

        self.__update_listbox()

    def __remove_item_button_click(self):
        selection = self.listbox.curselection()
        entry: str = self.listbox_values.get()[selection[0]]
        eid: ExerciseId = self.entries_to_items_binding[entry]

        self.remove_item_hook(eid)

        self.__update_listbox()

        self.update_buttons(main_selection=self.exercise_tree_frame.selected_eid)

    def __update_listbox(self):
        self.listbox.delete(0, tk.END)
        self.entries_to_items_binding.clear()

        sorted_selected_exercises: List[ExerciseId] = list(
            sorted([self.exercises[eid] for eid in self.get_items_hook()],
                   key=lambda item: item.name))
        for inx, exercise in enumerate(sorted_selected_exercises):
            entry: str = f'{exercise.name} (#{exercise.eid})'
            self.listbox.insert(tk.END, entry)
            self.entries_to_items_binding[entry] = exercise.eid

            if entry == self.listbox_selected_entry:
                self.listbox.selection_set(inx)

        if self.listbox_selected_entry not in self.entries_to_items_binding:
            self.listbox_selected_entry = None

        self.update_buttons(self.exercise_tree_frame.selected_eid)

    def update(self):
        self.__update_listbox()
        self.update_buttons(self.exercise_tree_frame.selected_eid)


class ExerciseHierarchyEditor:
    config_path = os.path.join('data', 'config.ini')

    def __init__(self, tk_controller: tk.Tk, logger: Logger, user: User,
                 exercises: ExercisesType,
                 edited_exercise: Exercise,
                 update_parent_hook: Callable
                 ):
        # TODO: Use events (i.e., observer pattern?)

        # A mapping of Listbox entries to their respective WordId-s.
        self.exercise_words_binding: Dict[str, WordId] = {}

        config = configparser.ConfigParser()
        config.read(self.config_path)

        self.user = user
        self.exercises = exercises
        self.exercises_repetition_config: ExercisesRepetitionConfigType = load_exercises_repetition_config(self.user)
        update_exercises_repetition_config(exercises_repetition_config=self.exercises_repetition_config,
                                           exercises=self.exercises,
                                           user=self.user)
        self.__update_parent_hook = update_parent_hook

        tree_frame_width = 400
        padding = 10

        exercise_frame_width = 500 + padding
        main_frame_width = tree_frame_width + exercise_frame_width
        frame_height = 500

        self.window = tk.Toplevel(tk_controller)
        # FIXME: In window's title add information about the edited exercise (e.g., its name).
        self.window.title(f'Hierarchy editor: {edited_exercise.name} (#{edited_exercise.eid})')
        self.window.minsize(width=main_frame_width, height=frame_height)
        self.window.maxsize(width=main_frame_width, height=frame_height)

        screen_width = tk_controller.winfo_screenwidth()
        screen_height = tk_controller.winfo_screenheight()
        self.window.geometry(f'{main_frame_width}x{frame_height}+{screen_width // 4}+{screen_height // 4}')

        self.window.protocol("WM_DELETE_WINDOW", self.__on_closing)

        self.tree_frame = ExerciseTreeFrame(
            self.window,
            exercises=self.exercises,
            exercises_repetition_config=self.exercises_repetition_config,
            item_clicked_hook=self.__on_treeview_item_clicked,
            height=500, width=tree_frame_width
        )
        self.tree_frame.grid(column=0, row=0, sticky='nesw', rowspan=2)

        # =======================

        hierarchy_list_frame_height = 200
        hierarchy_list_frame_width = 400

        # == < Parents frame > ==

        def get_parents_items_hook() -> Set[ExerciseId]:
            return {ex.eid for ex in self.exercises.values() if edited_exercise.eid in ex.subexercises}

        def add_parents_item_hook():
            self.tree_frame.selected_exercise.subexercises.add(edited_exercise.eid)
            self.tree_frame.build_exercise_tree()

        def remove_parents_item_hook(selected_eid: ExerciseId):
            parent_ex = self.exercises[selected_eid]
            parent_ex.subexercises.remove(edited_exercise.eid)
            self.tree_frame.build_exercise_tree()

        self.parents_frame = AddRemoveListFrame(self.window,
                                                text='Parents',
                                                exercises=self.exercises,
                                                exercise_tree_frame=self.tree_frame,
                                                get_items_hook=get_parents_items_hook,
                                                add_item_hook=add_parents_item_hook,
                                                remove_item_hook=remove_parents_item_hook,
                                                height=hierarchy_list_frame_height,
                                                width=hierarchy_list_frame_width,
                                                tk_controller=tk_controller)
        self.parents_frame.grid(column=1, row=0, sticky='nesw', padx=padding)

        def get_children_items_hook() -> Set[ExerciseId]:
            return edited_exercise.subexercises

        def add_children_item_hook() -> None:
            edited_exercise.subexercises.add(self.tree_frame.selected_eid)
            self.tree_frame.build_exercise_tree()

        def remove_children_item_hook(selected_eid: ExerciseId) -> None:
            edited_exercise.subexercises.remove(selected_eid)
            self.tree_frame.build_exercise_tree()

        self.children_frame = AddRemoveListFrame(self.window,
                                                 text='Children',
                                                 exercises=self.exercises,
                                                 exercise_tree_frame=self.tree_frame,
                                                 get_items_hook=get_children_items_hook,
                                                 add_item_hook=add_children_item_hook,
                                                 remove_item_hook=remove_children_item_hook,
                                                 height=hierarchy_list_frame_height,
                                                 width=hierarchy_list_frame_width,
                                                 tk_controller=tk_controller)
        self.children_frame.grid(column=1, row=1, sticky='nesw', padx=padding)

        # == </ Exercise summary > ==

        # self.tree_frame.tree.bind('<Button-1>', self.__on_treeview_item_clicked)

        # ====

        self.window.grid_columnconfigure(0, minsize=tree_frame_width, weight=1)
        self.window.grid_columnconfigure(1, minsize=exercise_frame_width, weight=0)
        self.window.grid_rowconfigure(0, minsize=hierarchy_list_frame_height, weight=1)
        self.window.grid_rowconfigure(1, minsize=hierarchy_list_frame_height, weight=1)

        # ====

        # Set the focus on dialog window (needed on Windows).
        self.window.focus_set()
        # Make sure events only go to our dialog.
        # self.window.grab_set()
        # Make sure dialog stays on top of its parent window (if needed).
        self.window.transient(tk_controller)

    def __on_closing(self):
        write_exercises(self.exercises, self.user)
        # FIXME: Implement - update parent window controls.
        self.__update_parent_hook()
        # self.main_frame.update_controls()
        self.window.destroy()

    def __on_treeview_item_clicked(self, event):
        # item_iid = self.tree_frame.tree.identify('item', event.x, event.y)
        # self.selected_exercise_iid = item_iid
        # if not item_iid and self.tree_frame.tree.get_children():
        #     self.selected_exercise_iid = self.tree_frame.tree.get_children()[-1]
        #     self.tree_frame.tree.focus(self.selected_exercise_iid)
        #     self.tree_frame.tree.selection_set(self.selected_exercise_iid)
        self.selected_exercise_iid = self.tree_frame.selected_exercise_iid

        # main_selection = self.selected_exercise_iid if self.selected_exercise_iid else None
        # self.parents_frame.update_buttons(main_selection)
        # self.children_frame.update_buttons(main_selection)
        self.parents_frame.update()
        self.children_frame.update()


if __name__ == '__main__':
    app = DummyApp()
    user = User(uid=0, name='Dummy')
    ex_conf_win = ExerciseConfigWindow(tk_controller=app, logger=app.logger, user=user,
                                       app=app, main_frame=None)

    # exercises: ExercisesType = load_exercises(user)
    #
    # dummy_exercise = exercises[2]
    # hierarchy_win = ExerciseHierarchyEditor(tk_controller=app, logger=app.logger, user=user,
    #                                         app=None, main_frame=None,
    #                                         exercises=exercises,
    #                                         edited_exercise=dummy_exercise)

    app.mainloop()

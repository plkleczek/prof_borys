import pickle
import os
import random
import tkinter as tk
import tkinter.font as font
from collections import deque
from datetime import datetime, timedelta
from itertools import count
from logging import Logger
from tkinter import ttk
from typing import Deque, Dict, List, Optional, TypeVar, Type

from PIL import ImageTk, Image

from main import WordId, read_repetitions_config, \
    VocabularyEntry, VocabularyType, Repetition, get_user_dir, User, overwrite_file, Filenames, \
    RepetitionsResultsType, VocabularyEntryRepetitionConfig, RepetitionsConfig, write_repetitions_results, \
    RepetitionsConfigType, VocabularyConfig, RepetitionErrorType, ExercisesRepetitionConfigType, ExercisesType, \
    RevisionStatus


def is_answer_correct(answer: str, word: VocabularyEntry):
    return answer in word.lang_l


def get_word_font():
    return font.Font(family='Helvetica', size=14)


def when_next_revision(word: VocabularyEntry, repetition_config: Optional[VocabularyEntryRepetitionConfig],
                       repetition_results: List[Repetition]) -> Optional[datetime]:
    # if not repetition_config.is_revised:
    #     raise RuntimeError

    repetition_results = sorted(repetition_results, key=lambda r: r.date)

    if not repetition_results:
        # The first repetition when starting to learn a given word
        # should occur immediately.
        return datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    if not repetition_results[-1].was_last_attempt_successful:
        # When a session was interrupted (i.e., the last attempt was unsuccessful)
        # the attempts in that session should be continued.
        return repetition_results[-1].date.replace(hour=0, minute=0, second=0, microsecond=0)

    base_interval = 3 - word.difficulty

    correct: List[bool] = [
        r.was_last_attempt_successful and (r.n_attempts == 1 or r.error_types == RepetitionErrorType.ACCENT) for r in
        repetition_results]
    streak = (correct[::-1] + [False]).index(False)

    # Later apply a penalty for each accent error during the most recent N revisions (in a streak).
    n_accent_errors_memory = 3
    was_accent_error: List[bool] = [
        r.was_last_attempt_successful and r.error_types == RepetitionErrorType.ACCENT for r in
        repetition_results]
    n_accent_errors = sum(was_accent_error[::-1][:n_accent_errors_memory])

    if repetition_config and repetition_config.modifier:
        modifier = VocabularyEntryRepetitionConfig.re_modifier.match(repetition_config.modifier)
        modifier_type = modifier.group('modifier_type')
        modifier_value = modifier.group('value')
        modifier_time_unit = modifier.group('time_unit')

        # FIXME: Consider the modifier when computing the interval.
        # if modifier_type == '+':
    else:
        pass

    dt_n_days = base_interval * 2 ** max(streak - RepetitionsConfig.min_streak + 1, 0)
    if n_accent_errors:
        # If there was an accent error, revise again quickly.
        dt_n_days = min(dt_n_days, 5)
    dt = timedelta(days=dt_n_days)

    # Prevent stacking of revisions on the same day.
    if dt.days > 30:
        dt += timedelta(days=random.randint(0, 3))

    t_next_revision = repetition_results[-1].date + dt
    if t_next_revision.hour < 3:
        t_next_revision = t_next_revision - timedelta(days=1)

    t_next_revision = t_next_revision.replace(hour=0, minute=0, second=0, microsecond=0)

    return t_next_revision


def get_revisions_schedule(vocabulary: VocabularyType,
                           exercises: ExercisesType,
                           repetitions_config: RepetitionsConfigType,
                           exercises_repetition_config: ExercisesRepetitionConfigType,
                           repetitions_results: RepetitionsResultsType
                           ) -> Dict[datetime, List[WordId]]:
    # FIXME: Compute which phrases are actually revised (based on both RepetitionsConfigType and ExercisesRepetitionConfigType).
    # FIXME: Implement "Use parent settings" option.
    words_revision_status: Dict[WordId, bool] = {wid: False for wid in vocabulary}

    for wid, entry in repetitions_config.items():
        if entry.is_revised:
            words_revision_status[wid] = True

    # Old version of the algorithm to determine which words to revise.
    parents = {eid: set() for eid in exercises}
    for eid, ex in exercises.items():
        for sub_ex_id in ex.subexercises:
            parents[sub_ex_id].add(eid)
    root_exercises = [ex for ex in exercises.values() if not parents[ex.eid]]

    # Perform a DFS search to determine which words should be revised.
    # If a parent exercise is 'skipped', none of its subexercises will be considered
    # - however, a given subexercise may still be considered if it is a part of
    # other exercise's hierarchy (and that exercise is set to be revised).
    stack = [ex.eid for ex in root_exercises]
    # print('-- Checked exercises:')
    while stack:
        eid = stack.pop(0)
        ex_rep_config = exercises_repetition_config[eid]
        if ex_rep_config.revision_status == RevisionStatus.REVISE:
            # print(eid)
            for wid in exercises[eid].words:
                words_revision_status[wid] = True
            stack = list(exercises[eid].subexercises) + stack
    # print('-- Finished --')

    # Old version of the algorithm to determine which words to revise.
    # for eid, entry in exercises_repetition_config.items():
    #     if entry.revision_status == RevisionStatus.REVISE:
    #         for wid in exercises[eid].words:
    #             words_revision_status[wid] = True

    next_revision_dates: Dict[WordId, datetime] = {wid: when_next_revision(word=vocabulary[wid],
                                                                           repetition_config=repetitions_config.get(
                                                                               wid),
                                                                           repetition_results=repetitions_results.get(
                                                                               wid, []))
                                                   for wid in vocabulary if words_revision_status[wid]}

    today = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    next_revision_dates = {wid: max(d, today) for wid, d in next_revision_dates.items()}

    schedule: Dict[datetime, List[WordId]] = {d: [] for d in set(next_revision_dates.values())}
    for wid, next_revision_date in next_revision_dates.items():
        schedule[next_revision_date].append(wid)

    return schedule


class RevisionsSession:
    frame_width: int = 860
    frame_height: int = 520

    # TODO: Actually in RevisionsSession.__init__() the `tk_parent` parameter should be of type app.MainFrame
    def __init__(self, tk_parent: ttk.Frame, tk_controller: tk.Tk, logger: Logger,
                 user: User, words_to_revise: List[WordId], repetitions: RepetitionsResultsType,
                 vocabulary: VocabularyType):

        self.window = tk.Toplevel(tk_controller)
        self.window.title('Revisions')
        self.window.minsize(width=self.frame_width, height=self.frame_height)
        self.window.maxsize(width=self.frame_width, height=self.frame_height)

        screen_width = tk_controller.winfo_screenwidth()
        screen_height = tk_controller.winfo_screenheight()
        self.window.geometry(f'{self.frame_width}x{self.frame_height}+{screen_width // 4}+{screen_height // 4}')

        self.__tk_frame = tk_parent
        self.__tk_controller = tk_controller
        self.logger = logger

        self.answers_counter = 0
        self.user = user
        self.repetitions = repetitions
        self.vocabulary = vocabulary

        self.__session_id = self.__get_session_id()

        repetitions_config = read_repetitions_config(self.user)
        # self.words_to_be_revised: Deque[WordId] = deque(
        #     [entry.wid for entry in repetitions_config.values() if
        #      entry.is_revised and when_next_revision(word=vocabulary[entry.wid],
        #                                              repetition_config=repetitions_config[entry.wid],
        #                                              repetition_results=repetitions[entry.wid]) <= datetime.now()])
        self.words_to_be_revised: Deque[WordId] = deque(words_to_revise)
        self.attempts: Dict[WordId, Repetition] = {
            wid: Repetition(date=datetime.now(), n_attempts=0, was_last_attempt_successful=False) for
            wid in
            self.words_to_be_revised}

        self.__bak_revision_file_inx_queue: Deque[int] = deque([1, 2])

        container = ttk.Frame(self.window)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (InputFrame, ResultFrame):
            page_name = F.__name__
            frame = F(tk_parent=container, tk_controller=tk_controller, session=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        # Set the focus on dialog window (needed on Windows).
        self.window.focus_set()
        # Make sure events only go to our dialog.
        self.window.grab_set()
        # Make sure dialog stays on top of its parent window (if needed).
        self.window.transient(tk_controller)

    def start_revisions(self):
        self.frames[InputFrame.__name__].tkraise()
        self.frames[InputFrame.__name__].next_repetition()

    @classmethod
    def __get_session_id(cls) -> str:
        return datetime.now().strftime('%Y%m%d%H%M%S')

    def finish_revisions(self):
        self.logger.debug('Revisions finished!!!')
        for wid, entry in self.attempts.items():
            if entry.n_attempts > 0:
                # FIXME: If an entry with the today's date already exists - just update it.
                if wid not in self.repetitions:
                    self.repetitions[wid] = []
                self.repetitions[wid].append(entry)

        temp_filepath = os.path.join(get_user_dir(self.user), 'temp',
                                     f'revision_{self.__session_id}.bak')

        write_repetitions_results(filepath=temp_filepath, repetition_results=self.repetitions)
        target_filepath = os.path.join(get_user_dir(self.user), Filenames.repetitions_filename)
        overwrite_file(src=temp_filepath, dst=target_filepath)

        # Remove all temporary files.
        for file_inx in self.__bak_revision_file_inx_queue:
            bak_file_path = self.__get_bak_file_path(file_inx=file_inx)
            if os.path.isfile(bak_file_path):
                os.remove(bak_file_path)

        self.window.destroy()
        self.__tk_frame.update_controls()

    def __get_bak_file_path(self, file_inx: int):
        return os.path.join(get_user_dir(self.user), 'temp',
                            f'rev_{self.__session_id}_{file_inx}.bak')

    def __rotate_bak_revision_file(self):
        with open(self.__get_bak_file_path(file_inx=self.__bak_revision_file_inx_queue[0]), 'wb') as bak_revision_file:
            pickle.dump(obj=self.attempts, file=bak_revision_file)

        prev_bak_file_path = self.__get_bak_file_path(file_inx=self.__bak_revision_file_inx_queue[-1])
        if os.path.isfile(prev_bak_file_path):
            os.remove(prev_bak_file_path)

        self.__bak_revision_file_inx_queue.rotate(-1)

    def give_answer(self, answer: str):
        revised_word = self.vocabulary[self.words_to_be_revised[0]]
        self.frames[ResultFrame.__name__].tkraise()
        self.frames[ResultFrame.__name__].show_result(word=revised_word, given_answer=answer)

    def next_repetition(self, answer: str, was_accepted: bool):
        self.answers_counter += 1

        self.attempts[self.words_to_be_revised[0]].n_attempts += 1
        self.attempts[self.words_to_be_revised[0]].date = datetime.now()

        revised_word: VocabularyEntry = self.vocabulary[self.words_to_be_revised[0]]

        if is_answer_correct(answer, revised_word) or was_accepted:
            self.logger.debug('Correct :)')
            self.attempts[self.words_to_be_revised[0]].was_last_attempt_successful = True
            self.words_to_be_revised.popleft()

            if not self.words_to_be_revised:
                self.finish_revisions()
                return
        else:
            def strip_accents(s: str) -> str:
                return s.replace(VocabularyConfig.accent_symbol, '')

            # Check if the only error is an erroneously placed accent...
            if strip_accents(answer) in [strip_accents(s) for s in revised_word.lang_l]:
                self.attempts[self.words_to_be_revised[0]].error_types |= RepetitionErrorType.ACCENT

            self.words_to_be_revised.rotate(-1)
            self.logger.debug('Incorrect :(')

        n_revisions_backup_interval = 5
        should_rotate_backup: bool = (self.answers_counter % n_revisions_backup_interval == 0)
        if should_rotate_backup:
            self.__rotate_bak_revision_file()

        self.frames[InputFrame.__name__].tkraise()
        self.frames[InputFrame.__name__].next_repetition()

    def get_stats_str(self) -> str:
        return f'{len(self.words_to_be_revised)} left / ' \
               f'attempt #{self.attempts[self.words_to_be_revised[0]].n_attempts}'


class InputFrame(ttk.Frame):
    def __init__(self, tk_parent: ttk.Frame, tk_controller: tk.Tk, session: RevisionsSession):
        super().__init__(tk_parent)
        self.controller = tk_controller

        self.__session = session

        word_frame_width = 600

        words_frame = ttk.Frame(self, height=120, width=word_frame_width)
        words_frame.controller = tk_controller
        words_frame.grid_propagate(False)

        self.word_to_revise_label = ttk.Label(words_frame, text='<word-to-revise>', anchor='w', width=word_frame_width)
        self.word_to_revise_label['font'] = get_word_font()

        self.answer_entry = ttk.Entry(words_frame, width=word_frame_width)
        self.answer_entry['font'] = get_word_font()

        revised_word_desc = ttk.Label(words_frame, text='RW:')
        given_answer_desc = ttk.Label(words_frame, text='GA:')

        word_pady = 5

        revised_word_desc.grid(column=0, row=0, sticky='s', pady=word_pady)
        self.word_to_revise_label.grid(column=1, row=0, sticky='sw', pady=word_pady)
        given_answer_desc.grid(column=0, row=1, sticky='s', pady=word_pady)
        self.answer_entry.grid(column=1, row=1, sticky='sw', pady=word_pady)

        words_frame.rowconfigure(0, minsize=40)
        words_frame.rowconfigure(1, minsize=40)

        self.stats_label = ttk.Label(self, text='- left')

        buttons_frame = ttk.Frame(self)
        buttons_frame.controller = tk_controller

        ok_button = ttk.Button(buttons_frame, text="OK")
        finish_button = ttk.Button(buttons_frame, text="Finish")

        def keydown_answer(event):
            if event.keysym in ('Return', 'KP_Enter'):
                self.__give_answer()

        def ok_click(event):
            self.__give_answer()

        def finish_click(event):
            self.__session.finish_revisions()

        self.answer_entry.bind('<KeyPress>', keydown_answer)  # FIXME: <Key> ?

        ok_button.bind('<Button-1>', ok_click)
        finish_button.bind('<Button-1>', finish_click)

        self.grid(column=0, row=0)

        row_inx = count()
        words_frame.grid(column=0, row=next(row_inx), columnspan=2, sticky='nw', padx=15)
        self.stats_label.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=20, pady=15)

        buttons_frame.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=10)
        ok_button.grid(column=0, row=0, padx=10)
        finish_button.grid(column=1, row=0)

        keyboard_image = ImageTk.PhotoImage(Image.open(os.path.join('resources', 'ru_keyboard_layout.png')))
        keyboard_label = tk.Label(self, image=keyboard_image)
        keyboard_label.image = keyboard_image
        keyboard_label.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=10)

    def set_word_to_revise_label_text(self):
        self.word_to_revise_label['text'] = self.__session.vocabulary[self.__session.words_to_be_revised[0]].lang_k

    def set_stats_label_text(self):
        self.stats_label['text'] = self.__session.get_stats_str()

    def next_repetition(self):
        self.set_word_to_revise_label_text()
        self.set_stats_label_text()
        self.answer_entry.focus_set()

    def __give_answer(self):
        answer = self.answer_entry.get()
        self.answer_entry.delete(0, tk.END)
        self.__session.give_answer(answer=answer)


class ResultFrame(ttk.Frame):
    """Present information about answer's correctness."""

    def __init__(self, tk_parent: ttk.Frame, tk_controller: tk.Tk, session: RevisionsSession):
        super().__init__(tk_parent)
        self.controller = tk_controller

        self.__session = session

        word_frame_width = 600

        words_frame = ttk.Frame(self, height=120, width=word_frame_width)
        words_frame.controller = tk_controller
        words_frame.grid_propagate(False)

        self.word_to_revise_label = ttk.Label(words_frame, text='<word-to-revise>', anchor='w', width=word_frame_width)
        self.word_to_revise_label['font'] = get_word_font()
        self.given_answer_label = ttk.Label(words_frame, text='<given-answer>', anchor='sw', width=word_frame_width)
        self.given_answer_label['font'] = get_word_font()
        self.model_answer_label = ttk.Label(words_frame, text='<model-answer>', anchor='w', width=word_frame_width)
        self.model_answer_label['font'] = get_word_font()

        revised_word_desc = ttk.Label(words_frame, text='RW:')
        given_answer_desc = ttk.Label(words_frame, text='GA:')
        model_answer_desc = ttk.Label(words_frame, text='MA:')

        word_pady = 5

        revised_word_desc.grid(column=0, row=0, sticky='s', pady=word_pady)
        self.word_to_revise_label.grid(column=1, row=0, sticky='sw', pady=word_pady)
        given_answer_desc.grid(column=0, row=1, sticky='s', pady=word_pady)
        self.given_answer_label.grid(column=1, row=1, sticky='sw', pady=word_pady)
        model_answer_desc.grid(column=0, row=2)
        self.model_answer_label.grid(column=1, row=2, sticky='w', pady=word_pady)

        words_frame.rowconfigure(0, minsize=40)
        words_frame.rowconfigure(1, minsize=40)

        self.stats_label = ttk.Label(self, text='- left')

        buttons_frame = ttk.Frame(self)
        buttons_frame.controller = tk_controller

        self.ok_button = ttk.Button(buttons_frame, text="OK")
        finish_button = ttk.Button(buttons_frame, text="Finish")
        accept_button = ttk.Button(buttons_frame, text="Accept")

        def ok_click(event):
            self.__proceed(was_accepted=False)

        def finish_click(event):
            self.__session.finish_revisions()

        def accept_click(event):
            self.__proceed(was_accepted=True)

        def ok_keydown(event):
            if event.keysym in ('Return', 'KP_Enter'):
                ok_click(event)

        self.ok_button.bind('<Button-1>', ok_click)
        self.ok_button.bind('<KeyPress>', ok_keydown)
        finish_button.bind('<Button-1>', finish_click)
        accept_button.bind('<Button-1>', accept_click)

        self.grid(column=0, row=0)

        row_inx = count()
        words_frame.grid(column=0, row=next(row_inx), columnspan=2, sticky='nw', padx=15)
        self.stats_label.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=20, pady=15)

        buttons_frame.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=10)
        self.ok_button.grid(column=0, row=0, padx=10)
        finish_button.grid(column=1, row=0)
        accept_button.grid(column=2, row=0)

        keyboard_image = ImageTk.PhotoImage(Image.open(os.path.join('resources', 'ru_keyboard_layout.png')))
        keyboard_label = tk.Label(self, image=keyboard_image)
        keyboard_label.image = keyboard_image
        keyboard_label.grid(column=0, row=next(row_inx), columnspan=2, sticky='w', padx=10)

    def show_result(self, word: VocabularyEntry, given_answer: str):
        self.word_to_revise_label['text'] = word.lang_k

        self.given_answer_label['text'] = given_answer or '(-)'
        self.model_answer_label['text'] = ''

        if is_answer_correct(answer=given_answer, word=word):
            self.given_answer_label.configure(foreground="black")
            self.model_answer_label['text'] = 'Correct!'
        else:
            self.given_answer_label.configure(foreground="red")
            self.model_answer_label['text'] = '/ '.join(word.lang_l)

        self.stats_label['text'] = self.__session.get_stats_str()

        self.ok_button.focus_set()

    def __proceed(self, was_accepted: bool):
        self.__session.next_repetition(answer=self.given_answer_label['text'],
                                       was_accepted=was_accepted)

import json
import logging
import os
import pickle
import pprint
import re
import shutil
from dataclasses import dataclass, field
from datetime import datetime
from enum import IntEnum, unique, Flag, Enum, auto
from re import Pattern
from typing import List, NewType, Dict, Set, ClassVar, Collection

GenericIdType = int
WordId = NewType('WordId', GenericIdType)
UserId = NewType('UserId', GenericIdType)
ExerciseId = NewType('ExerciseId', GenericIdType)


@unique
class Difficulty(IntEnum):
    EASY = 0
    MEDIUM = 1
    HARD = 2

    @classmethod
    def from_str(cls, diff_str: str):
        d = {k: v for v, k in cls.__members__.items()}
        return cls.__members__[d[int(diff_str)]]


@dataclass
class VocabularyEntry:
    wid: WordId
    """The **k**nown language."""
    lang_k: str
    """The **l**earnt language."""
    lang_l: List[str]
    difficulty: Difficulty


@dataclass
class User:
    uid: UserId
    name: str


@unique
class RepetitionErrorType(Flag):
    NONE = 0
    TYPO = 1
    ACCENT = 2


@dataclass
class Repetition:
    date: datetime
    n_attempts: int
    was_last_attempt_successful: bool
    error_types: RepetitionErrorType = RepetitionErrorType.NONE


class RepetitionsConfig:
    """Global configuration of repetitions' generation."""
    min_streak: int = 3


class VocabularyConfig:
    """Global configuration of repetitions' generation."""
    accent_symbol: str = '!'
    filename: str = 'vocabulary.dict'


@dataclass
class VocabularyEntryRepetitionConfig:
    wid: WordId
    is_revised: bool
    modifier: str = ''

    # FIXME: Perform proper checks (i.e., if *, then no time unit)
    re_modifier: ClassVar[Pattern] = re.compile(
        r'(?P<modifier_type>[\+>\*])(?P<value>\d+(?:\.\d+)?)(?P<time_unit>[dm])?')
    """Valid modifiers are of the following forms:
    * offset: +NU (after NU from the planned revision; cleared after the next revision)
    * interval: >NU (no sooner than after NU)
    * multiplier: *X (scale the current interval by X)
    where
    * ``N`` -- an integer
    * ``U`` -- a time unit (``d`` -- day, ``m`` -- month)
    * ``X`` -- a real number
     
    """


@dataclass
class Exercise:
    eid: ExerciseId
    name: str
    words: Set[WordId] = field(default_factory=set)
    subexercises: Set[ExerciseId] = field(default_factory=set)


class RevisionStatus(Enum):
    REVISE = auto()
    SKIP = auto()
    USE_PARENT_SETTINGS = auto()


@dataclass
class ExerciseRepetitionConfig:
    eid: ExerciseId
    revision_status: RevisionStatus = RevisionStatus.SKIP
    # modifier: str = ''


VocabularyType = NewType('VocabularyType', Dict[WordId, VocabularyEntry])
UsersType = NewType('UsersType', Dict[UserId, User])
RepetitionsResultsType = NewType('RepetitionsResultsType', Dict[WordId, List[Repetition]])
ExercisesType = NewType('ExercisesType', Dict[ExerciseId, Exercise])
RepetitionsConfigType = NewType('RepetitionsConfigType', Dict[WordId, VocabularyEntryRepetitionConfig])
ExercisesRepetitionConfigType = NewType('ExercisesRepetitionConfigType', Dict[ExerciseId, ExerciseRepetitionConfig])

logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=4)


def read_vocabulary(filepath: str) -> VocabularyType:
    """

    Input format (a text file with UTF-8 encoding):
      ``IDw: PL = RU1; RU2; ... #D |RU1'; RU2'; ...|``
    where
      * ``IDw`` -- a unique identifier of a word/phrase
      * ``PL`` -- the Polish word
      * ``RUi`` -- the Russian translation (many equivalent translations are acceptable)
      * ``D`` -- difficulty level (an integer)
      * ``RUi'`` -- the RUi translation, but with accent mark skipped

    The ``!`` character, standing in front of a vowel, denotes the accented syllable.

    Spaces next to the following characters---``:``, ``=``, ``;``, ``|``--- are optional.

    The "``|...|``" part is automatically generated (may not be present in the input file).

    :return:
    """

    re_entry_general = re.compile(
        r'(?P<wid>\d+) *: *(?P<pl_raw>[^=]+?) *= *(?P<ru_raw>[^=\|#]+) *#(?P<difficulty>\d+) *(?:\|.*\|)?',
        flags=re.UNICODE)

    # re_entry = re.compile(
    #     r'(?P<wid>\d+) *: *(?P<pl>[^=]+?) *= *(?P<ru_raw>([\w`][\w `]+)( *; *([\w`][\w `]+))*) *(?:\|.*\|)?', flags=re.UNICODE)

    vocabulary: VocabularyType = VocabularyType({})

    if not os.path.isfile(filepath):
        with open(file=filepath, mode='w', encoding='utf-8'):
            pass

    with open(file=filepath, mode='r', encoding='utf-8') as file:
        for line_inx, line0 in enumerate(file):
            line = line0.strip()
            result = re_entry_general.fullmatch(line)
            if not result:
                logger.error(f'Incorrect entry (L:{line_inx}): {line0}')

            wid = WordId(int(result.group('wid')))
            pl = result.group('pl_raw')
            ru = [chunk.strip() for chunk in result.group('ru_raw').split(';')]
            difficulty = Difficulty.from_str(result.group('difficulty'))

            vocabulary[wid] = VocabularyEntry(wid=wid, lang_k=pl, lang_l=ru, difficulty=difficulty)

    return vocabulary


def read_users(filepath: str) -> UsersType:
    """

    Input format (a text file with UTF-8 encoding):
      ``IDu = username``
    where
      * ``IDu`` -- a unique identifier of a user
      * ``username`` -- the user name

    :param filepath:
    :return:
    """
    re_entry_general = re.compile(r'(?P<uid>\d+) *= *(?P<username>\w+)', flags=re.UNICODE)

    users: UsersType = UsersType({})

    if not os.path.isfile(filepath):
        write_users(users=UsersType({}), filepath=filepath)

    with open(file=filepath, mode='r', encoding='utf-8') as file:
        for line_inx, line0 in enumerate(file):
            line = line0.strip()
            result = re_entry_general.fullmatch(line)
            if not result:
                logger.error(f'Incorrect entry (L:{line_inx}): {line0}')

            uid = UserId(int(result.group('uid')))
            if uid == 0:
                raise ValueError('UID=0 is reserved for the `dummy` user!')
            username = result.group('username')

            users[uid] = User(uid=uid, name=username)

    return users


def write_users(users: UsersType, filepath: str) -> None:
    """

    Output format (a text file with UTF-8 encoding):
      ``IDu = username``
    where
      * ``IDu`` -- a unique identifier of a user
      * ``username`` -- the user name

    :param users:
    :param filepath:
    """

    with open(file=filepath, mode='w', encoding='utf-8') as file:
        for uid, user in sorted(users.items()):
            file.write(f'{uid} = {user.name}\n')


def get_user_dir(user: User) -> str:
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(dir_path, 'data', 'users', str(user.uid))


class Filenames:
    repetitions_filename = 'repetitions.dat'
    exercises_filename = 'exercises.dat'
    repetitions_config_filename = 'repetitions_config.dat'
    exercises_repetition_config_filename = 'exercises_repetition_config.dat'


def load_repetitions_results(filepath: str) -> RepetitionsResultsType:
    byte_order = 'big'

    if not os.path.isfile(filepath):
        raise RuntimeError(f'No such file: {filepath}')

    with open(file=filepath, mode='rb') as file:
        repetition_results = RepetitionsResultsType({})
        n_repetition_results = int.from_bytes(file.read(8), byteorder=byte_order)
        for entry_inx in range(n_repetition_results):
            wid = WordId(int.from_bytes(file.read(8), byteorder=byte_order))
            n_repetitions = int.from_bytes(file.read(8), byteorder=byte_order)
            repetitions = []
            for repetition_inx in range(n_repetitions):
                date_int = int.from_bytes(file.read(5), byteorder=byte_order)
                date_year = date_int // 10 ** 8
                date_month = (date_int % 10 ** 8) // 10 ** 6
                date_day = (date_int % 10 ** 6) // 10 ** 4
                date_hour = (date_int % 10 ** 4) // 10 ** 2
                date_minute = date_int % 10 ** 2
                date_datetime = datetime(year=date_year, month=date_month, day=date_day,
                                         hour=date_hour, minute=date_minute)
                n_attempts = int.from_bytes(file.read(1), byteorder=byte_order)
                was_last_attempt_successful = bool.from_bytes(file.read(1), byteorder=byte_order)
                error_types = RepetitionErrorType(int.from_bytes(file.read(1), byteorder=byte_order))

                repetitions.append(Repetition(date=date_datetime,
                                              n_attempts=n_attempts,
                                              was_last_attempt_successful=was_last_attempt_successful,
                                              error_types=error_types))
            repetition_results[wid] = repetitions
        return repetition_results

        # return pickle.load(file)


def write_repetitions_results(repetition_results: RepetitionsResultsType, filepath: str) -> None:
    byte_order = 'big'

    with open(file=filepath, mode='wb') as file:
        file.write(len(repetition_results).to_bytes(length=8, byteorder=byte_order))
        for wid, repetitions in repetition_results.items():
            file.write(wid.to_bytes(length=8, byteorder=byte_order))
            file.write(len(repetitions).to_bytes(length=8, byteorder=byte_order))
            for repetition in repetitions:
                date_int = repetition.date.year * 10 ** 8 + repetition.date.month * 10 ** 6 \
                           + repetition.date.day * 10 ** 4 + repetition.date.hour * 10 ** 2 \
                           + repetition.date.minute
                file.write(date_int.to_bytes(length=5, byteorder=byte_order))
                file.write(repetition.n_attempts.to_bytes(length=1, byteorder=byte_order))
                file.write(repetition.was_last_attempt_successful.to_bytes(length=1, byteorder=byte_order))
                file.write(repetition.error_types.value.to_bytes(length=1, byteorder=byte_order))


def write_exercises(exercises: ExercisesType, user: User):
    exercises_out: List[Dict] = []
    for exercise in exercises.values():
        exercises_out.append({
            'id': exercise.eid,
            'name': exercise.name,
            'words': list(exercise.words),
            'subexercises': list(exercise.subexercises)
        })

    filepath = os.path.join(get_user_dir(user), Filenames.exercises_filename)
    with open(file=filepath, mode='w') as file:
        json.dump(exercises_out, file)


def load_exercises(user: User) -> ExercisesType:
    filepath = os.path.join(get_user_dir(user), Filenames.exercises_filename)
    if not os.path.isfile(filepath):
        raise RuntimeError(f'No such file: {filepath}')

    with open(file=filepath, mode='r') as file:
        data: List[Dict] = json.load(file)

    exercises: ExercisesType = {}
    for json_exercise in data:
        exercise = Exercise(eid=json_exercise['id'],
                            name=json_exercise['name'],
                            words=set(json_exercise['words']),
                            subexercises=set(json_exercise['subexercises']))
        exercises[exercise.eid] = exercise
    return exercises


def write_repetitions_config(repetitions_config: RepetitionsConfigType, filepath: str):
    with open(file=filepath, mode='wb') as file:
        pickle.dump(obj=repetitions_config, file=file)


def read_repetitions_config(user: User) -> RepetitionsConfigType:
    filepath = os.path.join(get_user_dir(user), Filenames.repetitions_config_filename)
    if not os.path.isfile(filepath):
        raise RuntimeError(f'No such file: {filepath}')

    with open(file=filepath, mode='rb') as file:
        return pickle.load(file)


def write_exercises_repetition_config(exercises_repetition_config: ExercisesRepetitionConfigType, user: User):
    exercises_config_out: List[Dict] = []
    for ex_config in exercises_repetition_config.values():
        exercises_config_out.append({
            'id': ex_config.eid,
            'status': str(ex_config.revision_status.name),
        })

    filepath = os.path.join(get_user_dir(user), Filenames.exercises_repetition_config_filename)
    with open(file=filepath, mode='w') as file:
        json.dump(exercises_config_out, file)


def update_exercises_repetition_config(exercises_repetition_config: ExercisesRepetitionConfigType,
                                       exercises: ExercisesType,
                                       user: User):
    for eid in exercises:
        if eid not in exercises_repetition_config:
            exercises_repetition_config[eid] = ExerciseRepetitionConfig(eid=eid)
    write_exercises_repetition_config(exercises_repetition_config=exercises_repetition_config,
                                      user=user)


def load_exercises_repetition_config(user: User) -> ExercisesRepetitionConfigType:
    filepath = os.path.join(get_user_dir(user), Filenames.exercises_repetition_config_filename)
    if not os.path.isfile(filepath):
        raise RuntimeError(f'No such file: {filepath}')

    with open(file=filepath, mode='r') as file:
        data: List[Dict] = json.load(file)

    exercises_config: ExercisesRepetitionConfigType = ExercisesRepetitionConfigType({})
    for json_ex_config in data:
        ex_config = ExerciseRepetitionConfig(
            eid=json_ex_config['id'],
            revision_status=RevisionStatus[json_ex_config['status']],
        )
        exercises_config[ex_config.eid] = ex_config
    return exercises_config


def overwrite_file(src: str, dst: str) -> None:
    """
    Overwrite the ``dst`` file with the ``src`` file (via *move*).

    For security reasons, perform the following steps:

    #. **Copy & rename** the ``src`` file to the target directory.
    #. **Rename** the ``dst`` file.
    #. **Rename** the ``src`` file in the target directory.
    #. **Delete** the ``src`` file in the source directory and the renamed ``dst`` file.

    :param src: path to the source file
    :param dst: path to the destination file
    """
    # dst_dir = os.path.dirname(dst)
    # os.path.basename(src)
    src_temp_name = src + '__TEMP'
    shutil.copy2(src=src, dst=src_temp_name)
    dst_temp_name = dst + '__TEMP'
    os.rename(src=dst, dst=dst_temp_name)
    os.rename(src=src_temp_name, dst=dst)
    os.remove(src)
    os.remove(dst_temp_name)


def get_lowest_id(existing_ids: Collection[int]) -> int:
    return max(existing_ids) + 1 if existing_ids else 0

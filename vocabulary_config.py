import configparser
import dataclasses
import os
import tkinter as tk
import copy
from logging import Logger
from tkinter import ttk
from typing import Dict, NamedTuple, Callable, List, Set, Optional, Tuple

from tkintertable import TableCanvas, TableModel, Filtering
from tkinter import *

from dummies import DummyApp
from main import VocabularyType, read_vocabulary, VocabularyConfig, ExercisesType, ExerciseId, Exercise, \
    VocabularyEntry, Difficulty, WordId, load_exercises, User, write_exercises, get_lowest_id, ExerciseRepetitionConfig, \
    ExercisesRepetitionConfigType


class ColumnMapping(NamedTuple):
    attribute: str
    column_name: str
    cast: Callable = str


word_id_attribute_name = 'wid'
wid_col_name = 'wid'
exercises_col_name = 'Exercises'

columns_mapping = [
    ColumnMapping(attribute=word_id_attribute_name, column_name=wid_col_name),
    ColumnMapping(attribute='lang_k', column_name='LK'),
    ColumnMapping(attribute='lang_l', column_name='LL'),
    ColumnMapping(attribute='difficulty', column_name='Diff', cast=int),
]

columns_lang = [e.column_name for e in columns_mapping if 'lang_' in e.attribute]


def get_word_exercises_as_str(wid: WordId, exercises: ExercisesType) -> str:
    s = {eid for eid, ex in exercises.items() if wid in ex.words}
    return ', '.join([str(e) for e in s]) if s else '-'


def vocabulary_to_table_data(vocabulary: VocabularyType, exercises: ExercisesType) -> Dict[str, Dict]:
    data = {}
    for wid, entry in vocabulary.items():
        rec_data = {}
        for itm in columns_mapping:
            raw_val = dataclasses.asdict(entry)[itm.attribute]
            val = itm.cast(raw_val)
            if 'lang_' in itm.attribute and isinstance(raw_val, list):
                val = '; '.join(raw_val)
            rec_data[itm.column_name] = val

        rec_data[exercises_col_name] = get_word_exercises_as_str(wid, exercises)

        data[wid] = rec_data
    return data


class VocabularyWindow:
    """Basic test frame for the table"""

    config_path = os.path.join('data', 'config.ini')
    re_exercise_listbox_entry = re.compile(r'.*\(#(?P<eid>\d+)\)')

    def __init__(self, tk_controller: tk.Tk, logger: Logger, user: User, app):
        # The initial state of the given exercise.
        self.exercise_playground: Optional[Exercise] = None

        # A mapping of Listbox entries to their respective WordId-s.
        self.exercise_words_binding: Dict[str, WordId] = {}

        config = configparser.ConfigParser()
        config.read(self.config_path)

        vocabulary_filename = config[app.mode]['vocabulary_file']
        self.vocabulary: VocabularyType = read_vocabulary(filepath=os.path.join('data', vocabulary_filename))
        self.user = user
        self.exercises: ExercisesType = load_exercises(self.user)

        vocabulary_frame_width = 600
        BUTTON_FRAME_WIDTH = 100
        SELECTED_WORD_LIST_WIDTH = 350
        padding = 10

        exercise_frame_width = BUTTON_FRAME_WIDTH + SELECTED_WORD_LIST_WIDTH + padding
        main_frame_width = vocabulary_frame_width + exercise_frame_width
        frame_height = 500

        self.window = tk.Toplevel(tk_controller)
        self.window.title('Vocabulary configuration')
        self.window.minsize(width=main_frame_width, height=frame_height)
        self.window.maxsize(width=main_frame_width, height=frame_height)
        screen_width = tk_controller.winfo_screenwidth()
        screen_height = tk_controller.winfo_screenheight()
        self.window.geometry(f'{main_frame_width}x{frame_height}+{screen_width // 4}+{screen_height // 4}')

        self.main = self.window

        words_frame = ttk.Frame(self.main, height=500, width=vocabulary_frame_width)
        # words_frame.pack(fill=BOTH, expand=True)
        # words_frame.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        words_frame.grid(column=0, row=0, sticky='nesw')

        search_frame = ttk.Frame(words_frame, height=35, width=vocabulary_frame_width)
        search_frame.controller = tk_controller
        search_frame.grid_propagate(False)

        search_label = ttk.Label(search_frame, text='Search:')
        search_label.grid(column=0, row=0, sticky='sw', padx=5, pady=5)

        sv_search_entry = StringVar()
        sv_search_entry.trace("w", lambda name, index, mode, sv=sv_search_entry: on_search_entry_changed(sv))
        # sv.trace_add("write", on_entry_changed)

        self.search_entry = ttk.Entry(search_frame, textvariable=sv_search_entry)

        def on_search_entry_changed(sv):
            text = sv.get()

            if text:
                recs = []
                for col_name in columns_lang:
                    # recs += self.table.model.filterBy(filtercol=col_name, op='starts with', value=text)
                    for key, row in self.table.model.data.items():
                        val: List[str] = row[col_name].split(';')

                        def matches(s: str, t: str) -> bool:
                            sx = s.strip()
                            return sx.startswith(t) or sx.replace(VocabularyConfig.accent_symbol, '').startswith(text)

                        if any([v for v in val if matches(v, text)]):
                            recs.append(key)

                self.table.model.filteredrecs = recs
                self.table.filtered = True

                self.add_word_button.configure(
                    state=tk.NORMAL if self.exercise_playground is not None and self.table.model.filteredrecs else tk.DISABLED)

            else:
                self.table.model.filteredrecs = None
                self.table.filtered = False

                self.add_word_button.configure(
                    state=tk.NORMAL if self.exercise_playground is not None and self.table.model.data else tk.DISABLED)

            # TODO: Change selection if not in filtered.
            self.table.setSelectedRow(0)
            self.table.clearSelected()

            self.table.redrawTable()

        self.search_entry.grid(column=1, row=0, sticky='sw', padx=5, pady=5)

        # fill_label = ttk.Label(search_frame, text='xx')
        # fill_label.grid(column=2, row=0, sticky='nesw', padx=5, pady=5)

        # search_frame.grid(row=0, column=0, sticky="nsew")
        search_frame.pack(anchor=W, padx=20)

        f = Frame(words_frame)
        f.pack(fill=BOTH, expand=True)
        # f.grid(row=1, column=0, sticky="nsew")

        self.table = TableCanvas(f,
                                 data=vocabulary_to_table_data(vocabulary=self.vocabulary,
                                                               exercises=self.exercises),
                                 read_only=True)
        self.table.bind('<ButtonRelease-1>', self.clicked)

        # table.createFilteringBar(parent=parent)

        # print(table.model.columnNames)
        # table.model.data[1]['a'] = 'XX'
        # table.model.setValueAt('YY',0,2)

        self.table.show()

        # x_label = ttk.Label(search_frame, text='X')
        # x_label.grid(column=0, row=0, sticky='sw', padx=5, pady=5)

        exercise_frame = ttk.Frame(self.main, height=500, width=BUTTON_FRAME_WIDTH + SELECTED_WORD_LIST_WIDTH)
        # exercise_frame.pack(side=tk.RIGHT, fill=tk.Y, expand=True)
        exercise_frame.grid(column=1, row=0, sticky='nesw', padx=padding)

        choose_exercise_frame = ttk.LabelFrame(exercise_frame, text='Exercise', height=200, width=exercise_frame_width)
        choose_exercise_frame.grid(column=0, row=0, columnspan=2, sticky='nesw', padx=padding, pady=padding)

        exercise_listbox_frame = ttk.Frame(choose_exercise_frame)
        exercise_listbox_frame.grid(row=0, column=0, columnspan=2, sticky='nesw')

        self.exercise_listbox_values = tk.Variable()
        self.exercise_listbox = tk.Listbox(exercise_listbox_frame, height=5, width=20,
                                           listvariable=self.exercise_listbox_values,
                                           exportselection=False)
        self.exercise_listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        # exercise_listbox.grid(row=0, column=0, columnspan=2, sticky='nesw')
        scrollbar = tk.Scrollbar(self.exercise_listbox)
        scrollbar.pack(side=tk.RIGHT, fill=tk.BOTH)

        self.__update_exercises_list()

        #
        self.exercise_listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.exercise_listbox.yview)
        #
        # listbox.grid(column=0, row=0, padx=10, pady=10)
        self.exercise_listbox.pack(fill=tk.BOTH)

        def exercise_listbox_select(event):
            selection: Tuple = event.widget.curselection()
            self.__exercise_listbox_select(selection)

        self.exercise_listbox.bind("<<ListboxSelect>>", exercise_listbox_select)

        self.sv_exercise_name = StringVar()
        exercise_name_entry = ttk.Entry(choose_exercise_frame, width=30, textvariable=self.sv_exercise_name)
        exercise_name_entry.grid(row=1, column=0, sticky='ew')

        # TODO: Typing = list filtering.
        def on_exercise_name_entry_changed(*args):
            add_exercise_button.configure(state=tk.NORMAL if self.sv_exercise_name.get() else tk.DISABLED)

        self.sv_exercise_name.trace("w", on_exercise_name_entry_changed)

        add_exercise_button = ttk.Button(choose_exercise_frame, text="Add", state=tk.DISABLED)
        add_exercise_button.grid(row=1, column=1)

        def add_exercise_button_click(event):
            new_exercise = Exercise(eid=get_lowest_id(self.exercises.keys()),
                                    name=self.sv_exercise_name.get(),
                                    words=set())

            self.exercises[new_exercise.eid] = new_exercise
            write_exercises(self.exercises, self.user)

            self.__update_exercises_list()
            self.__check_if_exercise_changed()

            # Select the newly added exercise.
            for inx, entry in enumerate(self.exercise_listbox_values.get()):
                if f'(#{new_exercise.eid})' in entry:
                    self.exercise_listbox.select_set(inx)

            self.exercise_playground = copy.deepcopy(new_exercise)
            self.__update_buttons()

        add_exercise_button.bind('<Button-1>', add_exercise_button_click)

        choose_exercise_frame.grid_rowconfigure(0, weight=1)
        choose_exercise_frame.grid_rowconfigure(1, weight=0)

        button_frame = ttk.Frame(exercise_frame, height=200, width=BUTTON_FRAME_WIDTH - padding)
        button_frame.controller = tk_controller
        button_frame.grid_propagate(False)
        # button_frame.pack(side=tk.LEFT)
        button_frame.grid(column=0, row=1, sticky='we', padx=padding)

        self.add_word_button = ttk.Button(button_frame, text=">", state=tk.DISABLED)
        self.remove_word_button = ttk.Button(button_frame, text="<", state=tk.DISABLED)
        self.clear_list_button = ttk.Button(button_frame, text="Clear", state=tk.DISABLED)
        self.save_button = ttk.Button(button_frame, text="Save", state=tk.DISABLED)

        self.add_word_button.grid(column=0, row=0)
        self.remove_word_button.grid(column=0, row=1, pady=padding)
        self.clear_list_button.grid(column=0, row=2, pady=padding)
        self.save_button.grid(column=0, row=3)

        def add_word_button_click(event):
            self.__add_word_button_click()

        def remove_word_button_click(event):
            self.__remove_word_button_click()

        def clear_word_button_click(event):
            self.__clear_word_button_click()

        def save_word_button_click(event):

            if self.exercise_playground is None:
                tk.messagebox.showerror("Error", "No exercise selected.")
                return

            self.__check_if_exercise_changed()

        self.add_word_button.bind('<Button-1>', add_word_button_click)
        self.remove_word_button.bind('<Button-1>', remove_word_button_click)
        self.clear_list_button.bind('<Button-1>', clear_word_button_click)
        self.save_button.bind('<Button-1>', save_word_button_click)

        self.exercise_words_listbox_values = tk.Variable()
        self.exercise_words_listbox = tk.Listbox(exercise_frame, height=10, width=20,
                                                 listvariable=self.exercise_words_listbox_values,
                                                 exportselection=False)
        # exercise_words_listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.exercise_words_listbox.grid(column=1, row=1, sticky='nesw', padx=padding)
        scrollbar = tk.Scrollbar(self.exercise_words_listbox)
        scrollbar.pack(side=tk.RIGHT, fill=tk.BOTH)
        self.__update_exercise_words_listbox()
        self.exercise_words_listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.exercise_words_listbox.yview)

        def exercise_words_listbox_select(event):
            selection: Tuple = event.widget.curselection()
            if selection:
                self.remove_word_button.configure(state=tk.NORMAL if selection else tk.DISABLED)

        self.exercise_words_listbox.bind("<<ListboxSelect>>", exercise_words_listbox_select)

        choose_exercise_frame.grid_propagate(False)
        exercise_frame.grid_propagate(False)

        exercise_frame.grid_columnconfigure(0, minsize=BUTTON_FRAME_WIDTH, weight=0)
        exercise_frame.grid_columnconfigure(1, minsize=SELECTED_WORD_LIST_WIDTH, weight=1)
        exercise_frame.grid_rowconfigure(0, minsize=200, weight=0)
        exercise_frame.grid_rowconfigure(1, minsize=200, weight=1)

        self.main.grid_columnconfigure(0, minsize=vocabulary_frame_width, weight=1)
        self.main.grid_columnconfigure(1, minsize=exercise_frame_width, weight=0)
        self.main.grid_rowconfigure(0, minsize=400, weight=1)

        return

    def clicked(self, event):
        try:
            row_inx = self.table.get_row_clicked(event)
            col_inx = self.table.get_col_clicked(event)

            n_rows = len(self.table.model.filteredrecs) if self.table.filtered else self.table.model.getRowCount()

            if row_inx >= n_rows or col_inx >= self.table.model.getColumnCount():
                return

            self.table.setSelectedRow(row_inx)
            self.table.setSelectedCells(row_inx, row_inx, col_inx, col_inx)

            # print('single cell:', self.table.model.getValueAt(row_inx, col_inx))
            # print('entire record:', self.table.model.getRecordAtRow(row_inx))

            self.table.redrawTable()
        except (KeyError, TypeError):
            print('Error')

    def __update_exercises_list(self):
        self.exercise_listbox.delete(0, tk.END)
        sorted_exercises = list(sorted(self.exercises.values(), key=lambda item: item.eid))
        for exercise in sorted_exercises:
            self.exercise_listbox.insert(tk.END, f'{exercise.name} (#{exercise.eid})')

    def __update_exercise_words_listbox(self):
        self.exercise_words_listbox.delete(0, tk.END)
        self.exercise_words_binding.clear()

        if self.exercise_playground:
            sorted_selected_words_ids = list(
                sorted({w.wid for w in self.vocabulary.values() if w.wid in self.exercise_playground.words},
                       key=lambda item: self.vocabulary[item].lang_l))
            for wid in sorted_selected_words_ids:
                word = self.vocabulary[wid]
                entry: str = f'{word.lang_l[0]} – {word.lang_k} (#{word.wid})'
                self.exercise_words_listbox.insert(tk.END, entry)
                self.exercise_words_binding[entry] = word.wid

        self.__update_buttons()

    def __was_exercise_changed(self) -> bool:
        """
        Check if current exercise modified.
        """
        if self.exercise_playground is None:
            return False

        return self.exercise_playground.words != self.exercises[self.exercise_playground.eid].words

    def __check_if_exercise_changed(self) -> None:
        """
        Check if current exercise modified; if so - show a dialog box asking whether to save changes (and save).
        """
        if self.__was_exercise_changed():
            MsgBox = tk.messagebox.askquestion('Save exercise',
                                               'The exercise was modified. Are you sure you want to save changes?',
                                               icon='warning')
            if MsgBox == 'yes':
                self.exercises[self.exercise_playground.eid] = self.exercise_playground
                write_exercises(self.exercises, self.user)

                # Update word's exercises column.
                for entry in self.table.model.data.values():
                    wid = WordId(int(entry[wid_col_name]))
                    entry[exercises_col_name] = get_word_exercises_as_str(wid, self.exercises)
                self.table.redraw()

    def __exercise_listbox_select(self, entry_ids: Tuple):
        self.__check_if_exercise_changed()

        # DEBUG
        if not entry_ids:
            pass

        selection: Tuple = self.exercise_listbox_values.get()
        if not selection:
            return

        entry: str = selection[entry_ids[0]]
        eid: ExerciseId = self.__extract_exercise_id_from_listbox_entry(entry)

        self.exercise_playground = copy.deepcopy(self.exercises[eid])
        self.__update_exercise_words_listbox()

    def __update_buttons(self):
        is_exercise_selected = self.exercise_playground is not None
        model = self.table.model

        is_vocabulary_entry_selected = model.filteredrecs is None and model.data or model.filteredrecs

        self.add_word_button.configure(
            state=tk.NORMAL if is_exercise_selected and is_vocabulary_entry_selected else tk.DISABLED)
        self.clear_list_button.configure(
            state=tk.NORMAL if is_exercise_selected and self.exercise_playground.words else tk.DISABLED)
        self.save_button.configure(
            state=tk.NORMAL if self.__was_exercise_changed() else tk.DISABLED)

        if not is_exercise_selected or not self.exercise_playground.words:
            self.remove_word_button.configure(state=tk.DISABLED)

    def __extract_exercise_id_from_listbox_entry(self, entry: str) -> ExerciseId:
        m = self.re_exercise_listbox_entry.fullmatch(entry)
        if m:
            return ExerciseId(int(m.group('eid')))
        else:
            raise ValueError(f'Incorrect entry: {entry}')

    def __add_word_button_click(self):
        if self.exercise_playground is None:
            tk.messagebox.showerror("Error", "No exercise selected.")
            return

        selected_record = self.table.get_currentRecord()
        added_word_id: WordId = WordId(int(selected_record[word_id_attribute_name]))
        self.exercise_playground.words.add(added_word_id)
        self.__update_exercise_words_listbox()

    def __remove_word_button_click(self):
        if self.exercise_playground is None:
            tk.messagebox.showerror("Error", "No exercise selected.")
            return

        selection = self.exercise_words_listbox.curselection()
        entry: str = self.exercise_words_listbox_values.get()[selection[0]]
        wid: WordId = self.exercise_words_binding[entry]

        self.exercise_playground.words.remove(wid)
        self.__update_exercise_words_listbox()

        # Clear search entry and navigate to the removed entry (in case someone clicked it by mistake).
        self.search_entry.delete(0, tk.END)
        #
        entry_inx = [entry[word_id_attribute_name] for entry in self.table.model.data.values()].index(str(wid))
        self.table.setSelectedRow(entry_inx)
        self.table.redrawTable()

        self.__update_buttons()

    def __clear_word_button_click(self):
        if self.exercise_playground is None:
            tk.messagebox.showerror("Error", "No exercise selected.")
            return

        if self.exercise_playground.words:
            MsgBox = tk.messagebox.askquestion('Clear exercise',
                                               'Are you sure you want to remove all words from this exercise?',
                                               icon='warning')
            if MsgBox == 'yes':
                self.exercise_playground.words.clear()
                self.__update_exercise_words_listbox()


if __name__ == '__main__':
    app = DummyApp()
    user = User(uid=0, name='Dummy')
    ex_conf_win = VocabularyWindow(tk_controller=app, logger=app.logger, user=user,
                                   app=app)
    app.mainloop()

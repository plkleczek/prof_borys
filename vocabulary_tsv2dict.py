import csv
import os
from pprint import PrettyPrinter
from typing import List

from main import VocabularyEntry, WordId, Difficulty, VocabularyConfig


def tsv2dict():
    def read_tsv_vocabulary() -> List[VocabularyEntry]:
        output_data: List[VocabularyEntry] = []
        full_file_path = os.path.join('data', 'vocabulary.tsv')
        with open(full_file_path, encoding='utf-8') as tsv_file:
            csv_reader = csv.reader(tsv_file, delimiter='\t', quotechar='"', doublequote=True)
            next(csv_reader)  # Skip header.
            for line_inx, row in enumerate(csv_reader, start=1):
                lang_l = [s for s in row[4:] if s]
                word = VocabularyEntry(wid=WordId(row[0]), lang_k=row[1], lang_l=lang_l,
                                       difficulty=Difficulty.from_str(row[3]))
                output_data.append(word)
        return output_data

    def vocabulary_entry_as_str(word: VocabularyEntry) -> str:
        lang_l = '; '.join(word.lang_l)
        lang_l_no_accents = lang_l.replace(VocabularyConfig.accent_symbol, '')
        return f"{word.wid}: {word.lang_k} = {lang_l} #{word.difficulty} |{lang_l_no_accents}|"

    words = read_tsv_vocabulary()
    # pp = PrettyPrinter(indent=4)
    # pp.pprint(words)

    dict_vocabulary_filepath = os.path.join('data', VocabularyConfig.filename)
    with open(dict_vocabulary_filepath, mode='w', encoding='utf-8') as dict_file:
        dict_file.writelines([vocabulary_entry_as_str(w) + '\n' for w in words])


tsv2dict()

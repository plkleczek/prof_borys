# README #

The _ProfBorys_ application is a program used to facilitate the process of learning new words/phrases in Russian.

Author: Paweł Kłeczek

## Manual ##

Run `app.py` script (the main script).

Create your user account if needed. To do so, choose _User > Switch user_.

Make sure the proper user is selected (_User > Switch user_). By default, if there is only one user defined, it is selected automatically.

### How to prepare the vocabulary? ###

Prepare the vocabulary as a UTF-8 encoded [TSV](https://en.wikipedia.org/wiki/Tab-separated_values) file (_data/vocabulary.tsv_). To facilitate the process, you may use a spreadsheet application (e.g., _OpenOffice Calc_, _MS Excel_).

Each entry should have the following format:  
``ID``<tab>``known_language``<tab>``#``<tab>``difficulty_level``<tab>``new_language``  
where

* ``ID`` -- a unique identifier of a word/phrase
* ``known_language`` -- the phrase in the known language
* ``difficulty_level`` -- difficulty level (an integer)
* ``new_language`` -- the translation of the ``known_language`` phrase

The ``!`` character, standing in front of a vowel (in the ``new_language`` phrase), denotes the accented syllable.

Then, in _ProfBorys_ application choose _Database > TSV2dict_ to convert _data/vocabulary.tsv_ file to _data/vocabulary.dict_ file (which is then read by _ProfBorys_ application).

### How to define exercises? ###

Choose _Database > Show vocabulary_.

To create an exercise, type its name in the text field and click _Add_ button.

Select the exercise you want to edit and then add/remove words/phrases you want to revise:

  * To add an item, select it in on the left list and click "_>_" button.
  * To remove an item, select it in on the right list and click "_<_" button.

After you are done with editing the set of words/phrases, make sure to save the changes -- to do so, click _Save_ button.

### How to select exercises to be practised? ###

Make sure you defined at least one exercise.

Choose _Database > Configure exercises_.

Select the exercise and choose the desired value in _Revision status_ combobox:

  * `Revise` -- the content of the exercise will be revised
  * `Skip` -- the content of the exercise will not be revised
  * `Use parent settings` -- (currently not supported)

Any changes are saved immediately and automatically.

### How to revise? ###

If you currently revise anything (i.e., at least one word is to be revised according to your exercises' settings), the revision schedule will be displayed on the main application screen.

Select the number of words to revise in one session and click _Revise_ button.

Type the translation of the `RW` word in `GA` text field and click _OK_ button (or press _ENTER_).

If the answer you provided was incorrect, you will see the proper answer (`MA`).
You may still accept your answer (e.g., if you made a typo) by clicking _Accept_ button.

To proceed to the next word, click _OK_ button.

You may finish the session at any time by clicking _Finish_ button.

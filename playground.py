import os
import pprint as pp
from datetime import datetime

from main import Difficulty, VocabularyEntry, WordId, VocabularyEntryRepetitionConfig, Repetition, \
    load_repetitions_results, User, read_vocabulary, read_users, load_exercises, \
    read_repetitions_config, get_user_dir, Filenames
from revisions import when_next_revision

# print(Difficulty.from_str('0'))

word1 = VocabularyEntry(wid=WordId(0), lang_k='x', lang_l=['X'], difficulty=Difficulty.EASY)
rep_conf = VocabularyEntryRepetitionConfig(wid=WordId(0), is_revised=True)
rep_results = [
    Repetition(date=datetime(2020, 10, 8), n_attempts=2, was_last_attempt_successful=False),
    Repetition(date=datetime(2020, 10, 5), n_attempts=1, was_last_attempt_successful=True),
    Repetition(date=datetime(2020, 10, 1), n_attempts=2, was_last_attempt_successful=True),
]

print('WNR: ', when_next_revision(word=word1, repetition_config=rep_conf, repetition_results=rep_results))


#
# e = RepetitionErrorType.TYPO | RepetitionErrorType.ACCENT
# print(e.value)

# dummy_user = User(uid=UserId(0), name='Dummy')
# pp.pprint(read_repetitions_results(dummy_user))

def set_up(user: User):
    root = get_user_dir(user)

    dir_path = os.path.join(root, 'temp')
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


voc = read_vocabulary(filepath=os.path.join('data', 'vocabulary.dict'))
# pp.pprint(voc)

users = read_users(filepath=os.path.join('data', 'users.conf'))
for user in users.values():
    set_up(user)
# pp.pprint(users)

current_user = users[0]

repetitions = load_repetitions_results(
    os.path.join(get_user_dir(current_user), Filenames.repetitions_filename))
pp.pprint(repetitions)

exercises = load_exercises(current_user)
# pp.pprint(exercises)

repetitions_config = read_repetitions_config(current_user)
# pp.pprint(repetitions_config)

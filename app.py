import argparse
import configparser
import getopt
import logging
import os
import shutil
import sys
import tkinter as tk
from datetime import datetime
from tkinter import ttk, messagebox
from tkinter.simpledialog import askstring

# from dummies import set_whole_vocabulary_to_be_revised
from typing import Optional

from exercises_config import ExerciseConfigWindow
from main import read_users, get_user_dir, VocabularyType, read_vocabulary, load_repetitions_results, load_exercises, \
    Filenames, read_repetitions_config, ExercisesType, write_exercises, write_repetitions_config, RepetitionsConfigType, \
    RepetitionsResultsType, write_repetitions_results, User, write_users, UserId, ExercisesRepetitionConfigType, \
    load_exercises_repetition_config, write_exercises_repetition_config, ExerciseRepetitionConfig, RevisionStatus
from revisions import RevisionsSession, get_revisions_schedule
from vocabulary_config import VocabularyWindow


class App(tk.Tk):
    """The application class."""

    config_path = os.path.join('data', 'config.ini')

    def __init__(self, mode='DEFAULT', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.winfo_toplevel().title(u'Profesor Borys  [by Paweł Kłeczek]')

        # FIXME: DEBUG
        # style = ttk.Style(self)
        # style.configure('TFrame', background='blue')

        self.logger = self.set_logger()

        self.mode = mode

        config = configparser.ConfigParser()
        config.read(self.config_path)

        users_filename = config[mode]['users_file']
        self.users_filepath = os.path.join('data', users_filename)
        self.users = read_users(filepath=self.users_filepath)
        if mode == 'DEBUG':
            self.users[0] = User(uid=UserId(0), name="Dummy")

        self.init_users()

        self.current_user: Optional[User] = None
        if mode == 'DEBUG':
            self.current_user = self.users[0]
        else:
            self.__try_select_first_user()

        vocabulary_filename = config[mode]['vocabulary_file']
        self.vocabulary: VocabularyType = read_vocabulary(filepath=os.path.join('data', vocabulary_filename))

        self.repetitions: RepetitionsResultsType = RepetitionsResultsType({})
        self.exercises: ExercisesType = ExercisesType({})
        self.repetitions_config: RepetitionsConfigType = RepetitionsConfigType({})
        self.exercises_repetition_config: ExercisesRepetitionConfigType = ExercisesRepetitionConfigType({})
        if self.current_user:
            self.load_user_data(self.current_user)

        # pp.pprint(revisions_schedule)
        # exit(-1)

        # The container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others.
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # frame_width: int = 350
        # frame_height: int = 200
        # self.minsize(width=frame_width, height=frame_height)
        # self.maxsize(width=frame_width, height=frame_height)
        # self.resizable(width=False, height=False)

        self.main_frame = MainFrame(tk_parent=container,
                                    tk_controller=self,
                                    logger=self.logger,
                                    user=self.current_user,
                                    repetitions=self.repetitions,
                                    vocabulary=self.vocabulary,
                                    exercises=self.exercises,
                                    repetitions_config=self.repetitions_config,
                                    exercises_repetition_config=self.exercises_repetition_config
                                    )

        # self.frames = {}
        # for F in (StartPage, PageOne, PageTwo):
        #     page_name = F.__name__
        #     frame = F(parent=container, controller=self)
        #     self.frames[page_name] = frame
        #
        #     # put all of the pages in the same location;
        #     # the one on the top of the stacking order
        #     # will be the one that is visible.
        #     frame.grid(row=0, column=0, sticky="nsew")
        #
        # self.show_frame("StartPage")

        self.menu_bar = tk.Menu(self)

        user_menu = tk.Menu(self.menu_bar, tearoff=0)
        user_menu.add_command(label="Switch user", command=self.switch_user)
        user_menu.add_command(label="Add user", command=self.add_user)

        self.menu_bar.add_cascade(label="User", menu=user_menu)

        database_menu = tk.Menu(self.menu_bar, tearoff=0)

        def tsv2dict_command():
            from vocabulary_tsv2dict import tsv2dict
            tsv2dict()
            messagebox.showinfo("Information", "The TSV dictionary file successfully converted to the .dict file.")
            self.vocabulary = read_vocabulary(filepath=os.path.join('data', vocabulary_filename))
            self.load_user_data(self.current_user)
            self.main_frame.update_controls()

        def show_vocabulary():
            vocab_win = VocabularyWindow(tk_controller=self, logger=self.logger, user=self.current_user,
                                         app=self)

        def configure_exercises():
            ex_conf_win = ExerciseConfigWindow(tk_controller=self, logger=self.logger, user=self.current_user,
                                               app=self, main_frame=self.main_frame)

        database_menu.add_command(label="TSV2dict", command=tsv2dict_command)
        database_menu.add_command(label="Show vocabulary", command=show_vocabulary)
        database_menu.add_command(label="Configure exercises", command=configure_exercises)
        # database_menu.add_command(label="Open", command=donothing)
        # database_menu.add_command(label="Save", command=donothing)
        # database_menu.add_separator()
        # database_menu.add_command(label="Exit", command=root.quit)
        self.menu_bar.add_cascade(label="Database", menu=database_menu)

        if self.current_user is None:
            self.menu_bar.entryconfig("Database", state='disabled')

        # helpmenu = Menu(menubar, tearoff=0)
        # helpmenu.add_command(label="Help Index", command=donothing)
        # helpmenu.add_command(label="About...", command=donothing)
        # menubar.add_cascade(label="Help", menu=helpmenu)

        self.config(menu=self.menu_bar)

    # def show_frame(self, page_name):
    #     '''Show a frame for the given page name'''
    #     frame = self.frames[page_name]
    #     frame.tkraise()

    def __try_select_first_user(self):
        if len(self.users) == 1:
            self.current_user = list(self.users.values())[0]

    def add_user(self):
        new_user_name = askstring(title="Add user...",
                                  prompt="Provide the new user name:")
        if new_user_name is None:
            return

        upper_uid_limit = max(self.users.keys()) if self.users.keys() else 0
        for uid in range(1, upper_uid_limit + 2):
            if uid not in self.users:
                self.users[uid] = User(uid=uid, name=new_user_name)
                write_users(users=self.users, filepath=self.users_filepath)
                self.init_users()
                self.__try_select_first_user()
                self.__update_controls()
                self.main_frame.update_controls()
                return

    def switch_user(self):
        window = tk.Toplevel(self)
        window.title('Switch user...')
        window.minsize(width=300, height=300)
        container = tk.Frame(window)
        container.pack(side="top", fill="both", expand=True)

        users_frame = ttk.LabelFrame(container, text='Users', height=100)
        users_frame.controller = window
        users_frame.pack_propagate(0)

        listbox = tk.Listbox(users_frame, height=5, width=20)
        listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scrollbar = tk.Scrollbar(listbox)
        scrollbar.pack(side=tk.RIGHT, fill=tk.BOTH)
        #
        sorted_users = list(sorted(self.users.values(), key=lambda item: item.name))
        for inx, user in enumerate(sorted_users):
            listbox.insert(tk.END, f'{user.name} (#{user.uid})')
            if user == self.current_user:
                listbox.select_set(inx)
        #
        listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=listbox.yview)

        # listbox.grid(column=0, row=0, padx=10, pady=10)
        listbox.pack(fill=tk.BOTH)

        # users_frame.grid(row=0, column=0)
        users_frame.pack(fill=tk.BOTH)

        # buttons_frame.grid(column=0, row=0, sticky='nw', padx=10)
        # n_words_to_revise_values_label.pack(fill=tk.X)
        # n_words_to_revise_combobox.pack(fill=tk.X)
        # revisions_button.pack(pady=20)

        buttons_frame = ttk.Frame(container)
        buttons_frame.controller = self

        ok_button = ttk.Button(buttons_frame, text="OK")
        cancel_button = ttk.Button(buttons_frame, text="Cancel")

        def ok_click(event):
            if not self.users:
                # FIXME: Gray out the options menu.
                window.destroy()
                return
            new_user_entry = listbox.get(listbox.curselection())
            uid = UserId(int(new_user_entry[new_user_entry.rindex('(') + 2:-1]))
            window.destroy()
            self.current_user = self.users[uid]
            self.load_user_data(self.current_user)
            self.main_frame.set_user(self.current_user)
            self.main_frame.update_controls()

            self.__update_controls()

        def cancel_click(event):
            window.destroy()

        ok_button.bind('<Button-1>', ok_click)
        cancel_button.bind('<Button-1>', cancel_click)

        # self.grid(column=0, row=0)

        # buttons_frame.grid(column=0, row=1, columnspan=2, sticky='w', padx=10)
        buttons_frame.pack(fill=tk.X)
        ok_button.grid(column=0, row=0, padx=10)
        cancel_button.grid(column=1, row=0)

        # ================================

        # Set the focus on dialog window (needed on Windows).
        window.focus_set()
        # Make sure events only go to our dialog.
        # window.grab_set()
        # Make sure dialog stays on top of its parent window (if needed).
        window.transient(self)
        # Display the window and wait for it to close.
        # window.wait_window(window)

    def __update_controls(self):
        self.menu_bar.entryconfig("Database", state='disabled' if self.current_user is None else 'normal')

    @classmethod
    def set_logger(cls):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s')  # %(name)s
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        return logger

    def init_users(self):
        for user in self.users.values():
            user_dir = get_user_dir(user)
            if not os.path.isdir(user_dir):
                os.makedirs(user_dir)

            temp_dir = os.path.join(get_user_dir(user), 'temp')
            shutil.rmtree(temp_dir, ignore_errors=True, onerror=None)  # FIXME: DEBUG
            if not os.path.isdir(temp_dir):
                os.makedirs(temp_dir)

            exercises_file = os.path.join(get_user_dir(user), Filenames.exercises_filename)
            if not os.path.isfile(exercises_file):
                write_exercises(exercises=ExercisesType({}), user=user)

            repetitions_config_file = os.path.join(get_user_dir(user),
                                                   Filenames.repetitions_config_filename)
            if not os.path.isfile(repetitions_config_file):
                write_repetitions_config(repetitions_config=RepetitionsConfigType({}),
                                         filepath=repetitions_config_file)

            exercises_repetition_config_file = os.path.join(get_user_dir(user),
                                                            Filenames.exercises_repetition_config_filename)
            if not os.path.isfile(exercises_repetition_config_file):
                write_exercises_repetition_config(exercises_repetition_config=ExercisesRepetitionConfigType({}),
                                                  user=user)

            repetitions_results_file = os.path.join(get_user_dir(user), Filenames.repetitions_filename)
            if not os.path.isfile(repetitions_results_file):
                write_repetitions_results(repetition_results=RepetitionsResultsType({}),
                                          filepath=repetitions_results_file)

    def load_user_data(self, user: User):
        self.repetitions.clear()
        self.repetitions.update(load_repetitions_results(
            os.path.join(get_user_dir(user), Filenames.repetitions_filename)))

        self.exercises.clear()
        self.exercises.update(load_exercises(user))

        self.repetitions_config.clear()
        self.repetitions_config.update(read_repetitions_config(user))

        self.exercises_repetition_config.clear()
        self.exercises_repetition_config.update(load_exercises_repetition_config(user))

        was_modified = False
        for eid, exercise in self.exercises.items():
            if eid not in self.exercises_repetition_config:
                self.exercises_repetition_config[eid] = ExerciseRepetitionConfig(
                    eid=eid, revision_status=RevisionStatus.SKIP
                )
                was_modified = True
        if was_modified:
            write_exercises_repetition_config(exercises_repetition_config=self.exercises_repetition_config,
                                              user=user)


class MainFrame(tk.Frame):
    @property
    def today_day(self):
        return datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    def __init__(self, tk_parent: ttk.Frame, tk_controller: tk.Tk, logger: logging.Logger,
                 user: User,
                 repetitions: RepetitionsResultsType,
                 vocabulary: VocabularyType,
                 exercises: ExercisesType,
                 repetitions_config: RepetitionsConfigType,
                 exercises_repetition_config: ExercisesRepetitionConfigType,
                 ):
        super().__init__(tk_parent)
        self.controller = tk_controller

        self.user = user
        self.repetitions = repetitions
        self.vocabulary = vocabulary
        self.exercises = exercises
        self.repetitions_config = repetitions_config
        self.exercises_repetition_config: ExercisesRepetitionConfigType = exercises_repetition_config

        self.revisions_schedule = None

        buttons_frame = ttk.Frame(self)
        buttons_frame.controller = tk_controller

        n_words_to_revise_values_label = ttk.Label(buttons_frame, text='# words to revise per session:')

        self.n_words_to_revise_combobox = ttk.Combobox(buttons_frame, state="readonly",
                                                       values=[], postcommand=self.update_controls)

        def start_revisions():
            n_words_to_revise = int(self.n_words_to_revise_combobox.get())
            revisions_session = RevisionsSession(tk_parent=self,
                                                 tk_controller=self.controller,
                                                 logger=logger,
                                                 user=self.user,
                                                 words_to_revise=self.revisions_schedule[self.today_day][
                                                                 :n_words_to_revise],
                                                 repetitions=self.repetitions,
                                                 vocabulary=self.vocabulary)
            revisions_session.start_revisions()
            # self.tkraise()

        self.revisions_button = ttk.Button(buttons_frame, text="Revise!", command=start_revisions)

        schedule_frame = ttk.LabelFrame(self, text='Schedule')
        schedule_frame.controller = tk_controller

        self.listbox = tk.Listbox(schedule_frame)
        self.listbox.pack(side=tk.LEFT, fill=tk.BOTH)
        scrollbar = tk.Scrollbar(schedule_frame)
        scrollbar.pack(side=tk.RIGHT, fill=tk.BOTH)
        self.listbox.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.listbox.yview)

        self.grid(column=0, row=0, padx=10, pady=10)

        buttons_frame.grid(column=0, row=0, sticky='nw', padx=10)
        n_words_to_revise_values_label.pack(fill=tk.X)
        self.n_words_to_revise_combobox.pack(fill=tk.X)
        self.revisions_button.pack(pady=20)

        schedule_frame.grid(column=1, row=0)

        # revisions_button.bind('<Button-1>', revisions_click)

        self.update_controls()

    def set_user(self, user: User):
        self.user = user

    def update_controls(self):
        self.revisions_schedule = get_revisions_schedule(vocabulary=self.vocabulary,
                                                         exercises=self.exercises,
                                                         repetitions_config=self.repetitions_config,
                                                         exercises_repetition_config=self.exercises_repetition_config,
                                                         repetitions_results=self.repetitions)

        batch_sizes = self.prepare_list_of_revision_batch_sizes(self.revisions_schedule)
        self.n_words_to_revise_combobox['values'] = batch_sizes
        self.n_words_to_revise_combobox.current(0)

        self.listbox.delete(0, tk.END)
        for rev_date in sorted(self.revisions_schedule):
            self.listbox.insert(tk.END, f'{rev_date.strftime("%Y-%m-%d")} :  {len(self.revisions_schedule[rev_date])}')

        self.revisions_button.configure(state=tk.NORMAL if self.today_day in self.revisions_schedule else tk.DISABLED)

        self.update()

    def prepare_list_of_revision_batch_sizes(self, revisions_schedule):
        n_words_min = 20
        n_words_interval = 20
        n_words_max = 100
        n_words_to_revise_today = len(revisions_schedule[self.today_day]) if self.today_day in revisions_schedule else 0
        n_words_to_revise_values = list(range(min(n_words_to_revise_today, n_words_min),
                                              min(n_words_to_revise_today, n_words_max) + 1,
                                              n_words_interval))
        if n_words_to_revise_today < n_words_max and n_words_to_revise_today not in n_words_to_revise_values:
            n_words_to_revise_values.append(n_words_to_revise_today)
        return n_words_to_revise_values


def set_russian_keyboard():
    # import sys
    # import ctypes
    # from ctypes import wintypes
    #
    # KLF_ACTIVATE = 0x00000001
    #
    # RUSSIAN_LOCALE_ID_BYTES = b"00000419"
    # # locale_id_bytes = b"00000409"
    # klid = ctypes.create_string_buffer(RUSSIAN_LOCALE_ID_BYTES)
    # user32_dll = ctypes.WinDLL("user32")
    # kernel32_dll = ctypes.WinDLL("kernel32")
    # LoadKeyboardLayout = user32_dll.LoadKeyboardLayoutA
    # LoadKeyboardLayout.argtypes = [wintypes.LPCSTR, wintypes.UINT]
    # LoadKeyboardLayout.restype = wintypes.HKL
    # GetLastError = kernel32_dll.GetLastError
    # GetLastError.restype = wintypes.DWORD
    #
    # klh = LoadKeyboardLayout(klid, KLF_ACTIVATE)
    # print("{0:s} returned: 0x{1:016X}".format(LoadKeyboardLayout.__name__, klh))
    # print("{0:s} returned: {1:d}".format(GetLastError.__name__, GetLastError()))
    pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', choices=('DEFAULT', 'DEBUG'), default='DEFAULT')
    args = parser.parse_args()

    app = App(mode=args.mode)

    set_russian_keyboard()

    app.mainloop()
